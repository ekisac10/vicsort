#! /usr/bin/env python3

from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.server.sync import ModbusTcpServer, ModbusSocketFramer
from pymodbus.datastore import ModbusSequentialDataBlock
from pymodbus.framer.socket_framer import ModbusSocketFramer
from scapy.contrib.modbus import ModbusADUResponse
from scapy.all import *
from src.dataset_manager import DataSetManager, generate_input_values
import random
import signal
import sys
import struct

"""
This script is designed to run with pymodbus version 2.5.2.
This script receives Modbus Requests from the HMI and generates Modbus Responses to each of 
those requests, and sends them back to the HMI.

The script allows the user to customise the values sent back to the HMI and even send back values within 
a specific range for a more realistic feel.
"""

MODBUS_SERVER_PORT = 502
MODBUS_SERVER_IP = "192.168.90.197"

def updating_writer():
    """
    Ensure to supply values within the range for each parameter. The spread must also be within range of the parameter.
    Otherwise, a 0 value is returned. Also, if the value is too low, a 0 is returned.
    This would be more efficient with a bigger dataset.
    """
    values = generate_input_values(feed1_perc=20,
                                   feed1_press=300,
                                   feed2_perc=98,
                                   feed2_press=40,
                                   pressure=2254.4,
                                   level=35.0,
                                   purge_perc=30,
                                   purge_press=20.0,
                                   product_press=31.8,
                                   product_perc=15.0,
                                   level_spread=0.4,
                                   pressure_spread=20,
                                   product_perc_spread=20,
                                   product_press_spread=20,
                                   feed2_perc_spread=2,
                                   feed2_press_spread=10,
                                   feed1_perc_spread=2,
                                   feed1_press_spread=50,
                                   purge_perc_spread=15,
                                   purge_press_spread=10
                                   )
    print("The custom register values are:", values)
    return values


def signal_handler(sig, frame):
    """ Handle the received signal """
    print("Shutting down the server gracefully...")
    server.server_close()  # Close the server
    print("Server shutdown complete. Exiting...")
    sys.exit(0)


class MBTCP(Packet):
    """
    Neccessary for printing the generated Modbus Response in a Scapy format
    """
    name = "ModbusTCP"
    fields_desc = [ShortField("transId", 0),
                   ShortField("protoId", 0),
                   ShortField("len", 0),
                   ByteField("unitId", 0)
                   ]


class MODBUS(Packet):
    """
    Neccessary for printing the generated Modbus Response in a Scapy format
    """
    list_ = [6553, 6553, 0, 0, 0, 0, 10476, 4452, 55245, 28955, 30993, 6376, 12345]
    name = "Modbus"
    fields_desc = [XByteField("funcCode", 0x04),
                   BitFieldLenField("byteCount", None, 8,
                                    count_of="registerVal",
                                    adjust=lambda pkt, x: x * 2),
                   FieldListField("registerVal", list_,
                                  ShortField("", 0x0000),
                                  count_from=lambda pkt: pkt.byteCount)]


class CustomSocketFramer(ModbusSocketFramer):
    def buildPacket(self, message):
        """Create a ready to send modbus packet.

        :param message: The populated request/response to send
        """
        # Call the parent class's buildPacket method
        packet = super(CustomSocketFramer, self).buildPacket(message)
        print(f"Sending Response: {packet}")

        # Print the message and data for diagnostic purposes
        bind_layers(MBTCP, MODBUS)
        decoded_packet = MBTCP(packet)
        decoded_packet.show()
        return packet


class CustomDataBlock(ModbusSequentialDataBlock):
    def getValues(self, address, count=1):
        """ Override the default getValues method to print a notification and update the values """
        print(f"\nReceived Modbus request for address {address} and count {count}")
        # Update the values using the updating_writer function
        updated_values = updating_writer()
        self.values[:len(updated_values)] = updated_values
        return super(CustomDataBlock, self).getValues(address, count)

    def setValues(self, address, values):
        """ Override the default setValues method to set a predefined list of values """
        # You can adjust the values here as needed
        predefined_values = [6553, 6553, 0, 0, 0, 0, 10476, 4452, 51245, 28975, 30993, 6376, 12345, 0]
        super(CustomDataBlock, self).setValues(address, predefined_values)


"""
Discrete Inputs (di): These are read-only binary inputs, typically representing digital inputs to a device.
Coils (co): These are binary outputs or relay outputs. You can read and write to them.
Holding Registers (hr): These are typically used to store analog outputs or configuration data that can be read and written to. They represent 16-bit data registers.
Input Registers (ir): These are used for analog inputs. They are read-only and also represent 16-bit data registers.

The server's data store (coils, discrete inputs, input registers, and holding registers) is initialized with the value 17 for 100 addresses in each block
"""

def main():
    store = ModbusSlaveContext(
        di=CustomDataBlock(0, [17] * 100),
        co=CustomDataBlock(0, [17] * 100),
        hr=CustomDataBlock(0, [17] * 100),
        ir=CustomDataBlock(1, updating_writer())
    )
    context = ModbusServerContext(slaves=store, single=True)

    # Register the signal handlers
    signal.signal(signal.SIGINT, signal_handler)  # Handle CTRL+C
    signal.signal(signal.SIGTSTP, signal_handler)  # Handle CTRL+Z

    print("Starting Custom Modbus Server...\n")
    server = ModbusTcpServer(context, address=(MODBUS_SERVER_IP, MODBUS_SERVER_PORT), framer=CustomSocketFramer)
    server.serve_forever()

if __name__ == "__main__":
    main()
