#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
from pymodbus.client import ModbusTcpClient


"""
This script is designed to run with pymodbus 3.5.4.
However, it should work with higher versions too.
"""

"""
modbus_coils.py

Description:
  This script provides functionality to read from or write to Modbus coils.
  It supports reading the value(s) from one or multiple coils and writing a value
  to one or multiple coils.

Usage: modbus_coils.py [command] [arguments]

Commands:
  read_coils <starting_coil> <number_of_coils_to_read>
  write_coils <value_to_write> <starting_coil> <number_of_coils_to_write>

read_coils:
  Reads the value(s) from one or multiple Modbus coils.

  Arguments:
    starting_coil          The first coil to read from (integer).
    number_of_coils_to_read The number of consecutive coils to read (integer).

  Examples:
    modbus_coils.py read_coils 40 1    # Reads the value of coil 40.
    modbus_coils.py read_coils 40 10   # Reads values of coils from 40 to 50.

write_coils:
  Writes a value to one or multiple Modbus coils.

  Arguments:
    value_to_write         The value to write (True or False).
    starting_coil          The first coil to write to (integer).
    number_of_coils_to_write The number of consecutive coils to write to (integer).

  Examples:
    modbus_coils.py write_coils True 40 1  # Writes the value '1' to coil 40.
    modbus_coils.py write_coils True 40 5  # Writes the value '1' to coils from 40 to 45.

Note: 
- Ensure that the client is correctly configured and connected before running these commands.
- Use integer values for coil numbers and the number of coils to read/write.
- For writing coils, use 'True' to write '1' and 'False' to write '0'.

Author: Conrad Ekisa
Created: Nov 2023
Last Modified: Nov 2023
Version: 1.0

License: European Union Public License v1.2 (EUPLv1.2)
"""


# Modbus server details
MODBUS_SERVER_IP = '192.168.95.2'
MODBUS_SERVER_PORT = 502  # Default port for Modbus TCP


def read_coils(client_connection: ModbusTcpClient, data_address: int = 0, number: int = 100):
    """
    Reads a range of Modbus coils starting from a specified address.

    :param client_connection: The Modbus client connection.
    :param data_address: The starting address for the read operation (default is 0).
    :param number: The number of consecutive coils to read starting from data_address (default is 100).

    Example:
    To read the first 100 coils, set data_address to 0 and number to 100.
    """

    # Read coils
    response = client_connection.read_coils(data_address, number)
    valid_coils = response.bits[:number]

    # Check if the response is valid
    if not response.isError():
        # Successful response
        print("\nCoils status:")
        for i, coil in enumerate(valid_coils, start=data_address):
            print(f"Coil {i}: {coil}")
    else:
        # Error handling
        print(f"Failed to read coils: {response}")


def write_coils(client_connection: ModbusTcpClient, value: bool, data_address: int = 0, number: int = 100):
    """
    Writes a boolean value to one or more Modbus coils.

    :param client_connection: The Modbus client connection.
    :param value: The boolean value to write (True or False).
    :param data_address: The starting address for writing (default is 0).
    :param number: The number of consecutive coils to write to (default is 100).
    """

    # Create a list of True values to set coils from data_address - number to 1
    values_to_write = [value] * number

    write_response = client_connection.write_coils(data_address, values_to_write)

    # Check if the response is valid
    if not write_response.isError():
        # Successful response
        print(f"\nSuccessfully wrote a value of: {value}, to coils: {write_response}")
    else:
        # Error handling
        print(f"Failed to write to coils: {write_response}")


def read_input_registers(client_connection: ModbusTcpClient, data_address: int = 0, number=1):
    """
    Reads one or more Modbus input register values starting from a specified address.
    Below is a reference list:
    list_ = [65240, 6356, 0, 0, 0, 0, 13345, 5182, 54459, 29074, 0, 0, 0]

    :param client_connection: The Modbus client connection.
    :param data_address: The index address of the register to start reading from (default is 0).
    :param number: The number of registers to read starting from the data_address (default is 1).

    Example:
    To read the 'pressure' value that is stored at index 8, set data_address to 8.
    To read 'pressure' and the next register, set data_address to 8 and number to 2.
    """

    response = client_connection.read_input_registers(data_address, number)
    if not response.isError():
        print(f"Register values starting at index/register {data_address} with number set to {number}:",
              response.registers)
    else:
        print("Error reading registers")


def write_register(client_connection: ModbusTcpClient, value_to_write: int, data_address: int = 0):
    response = client_connection.write_register(data_address, value_to_write)

    # Check if the response is valid
    if not response.isError():
        # Successful write
        print(f"Register {data_address} written successfully with value {value_to_write}")
    else:
        # Error handling
        print(f"Failed to write register: {response}")


def str_to_bool(value):
    if value.lower() in ('true', '1', 't', 'y', 'yes'):
        return True
    elif value.lower() in ('false', '0', 'f', 'n', 'no'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')


def main():
    parser = argparse.ArgumentParser(
        description="Modbus Coils Utility\n\n"
                    "This script provides functionality to read from or write to Modbus coils. "
                    "\nIt supports reading the value(s) from one or multiple coils and writing a value "
                    "to one or multiple coils."
                    "\nSet the Modbus server and port within the script.",
        epilog="Examples:\n"
               "  modbus_coils.py read_coils 40 1    # Reads the value of coil 40.\n"
               "  modbus_coils.py read_coils 40 10   # Reads values of coils from 40 to 50.\n"
               "\n  modbus_coils.py write_coils True 40 1  # Writes the value '1' to coil 40.\n"
               "  modbus_coils.py write_coils False 40 5  # Writes the value '0' to coils from 40 to 45.",
        formatter_class=argparse.RawDescriptionHelpFormatter
    )

    subparsers = parser.add_subparsers(dest='command', help='Commands')

    # Read Coils Parser
    read_parser = subparsers.add_parser('read_coils', help='Read the value(s) from Modbus coils')
    read_parser.add_argument('starting_coil', type=int, help='The first coil to read from')
    read_parser.add_argument('number_of_coils_to_read', type=int, help='The number of consecutive coils to read')

    # Write Coils Parser
    write_parser = subparsers.add_parser('write_coils', help='Write a value to Modbus coils')
    write_parser.add_argument('value_to_write', type=str_to_bool, help='The value to write (True or False)')
    write_parser.add_argument('starting_coil', type=int, help='The first coil to write to')
    write_parser.add_argument('number_of_coils_to_write', type=int, help='The number of consecutive coils to write to')

    args = parser.parse_args()

    # Connect to the Modbus server
    client = ModbusTcpClient(MODBUS_SERVER_IP, port=MODBUS_SERVER_PORT)
    client.connect()

    if args.command == 'read_coils':
        try:
            read_coils(client, args.starting_coil, args.number_of_coils_to_read)
        except ModbusException as e:
            print(f"Modbus error: {e}")
    elif args.command == 'write_coils':
        try:
            write_coils(client, args.value_to_write, args.starting_coil, args.number_of_coils_to_write)
            read_coils(client, args.starting_coil, args.number_of_coils_to_write)
        except ModbusException as e:
            print(f"Modbus error: {e}")
    else:
        parser.print_help()


if __name__ == '__main__':
    main()
