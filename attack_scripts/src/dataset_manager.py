#! /usr/bin/python3

import random

"""
This file takes the samples register input values and their corresponding outputs as seen from HMI and uses
mathematical linear interpolation to determine the input value depending on the output value you would like to see on
the HMI.

"""


class DataSet:
    def __init__(self, data):
        self.data = data
        self.keys = list(data.keys())
        self.values = list(data.values())

    def apply_spread(self, base_value, spread):
        if spread > 0:
            return base_value + random.uniform(0, spread)
        elif spread < 0:
            return base_value + random.uniform(spread, 0)
        else:
            return base_value

    def get_input_value(self, output_value, spread=0.0):
        """
        Given an output value, this method returns the corresponding input value.
        If a spread value is provided, it will adjust the output value either upwards or downwards randomly
        within the range defined by the spread before determining the input value.

        Parameters:
        - output_value: The value for which you want to find the corresponding input value.
        - spread (default = 0.0): The range within which to adjust the output value.
          - Positive spread: Increases the output value randomly within the spread range.
          - Negative spread: Decreases the output value randomly within the spread range.

        For example:
        If output_value = 61.9 and spread = 5.0, the actual output value used
        for the lookup will be between 61.9 and 66.9 (inclusive).

        If output_value = 61.9 and spread = -5.0, the actual output value used
        for the lookup will be between 56.9 and 61.9 (inclusive).

        Returns:
        The corresponding input value based on the provided or adjusted output value.
        """

        # Apply the spread to the provided output value
        spread_applied_value = self.apply_spread(output_value, spread)

        # First, we'll check if the applied value exists in the dataset
        for key, value in self.data.items():
            if abs(value - spread_applied_value) <= 0.01:  # Checking with a small epsilon for float comparison
                return key

        # If the value isn't in the provided dataset, we'll extrapolate
        for i in range(1, len(self.values)):
            if self.values[i - 1] <= spread_applied_value <= self.values[i]:
                fraction = (spread_applied_value - self.values[i - 1]) / (self.values[i] - self.values[i - 1])
                extrapolated_value = self.keys[i - 1] + fraction * (self.keys[i] - self.keys[i - 1])
                return int(extrapolated_value)  # Cast the value to an integer

        # In case no value can be determined
        return 0


class DataSetManager:
    def __init__(self):
        self.datasets = {
            'feed2_percentage': DataSet({
                15534: 23.7,
                25534: 38.9,
                35534: 52.2,
                45534: 69.5,
                55534: 84.7,
                65534: 100.0
            }),
            'feed2_pressure': DataSet({
                2553: 19.7,
                3553: 26.9,
                4553: 35.0,
                5553: 42.2,
                6553: 50.0
            }),
            'feed1_percentage': DataSet({
                5000: 7.6,
                15000: 22.9,
                25000: 38.2,
                35000: 53.5,
                45000: 68.7,
                55000: 83.9,
                65000: 99.2
            }),
            'feed1_pressure': DataSet({
                5000: 38.4,
                15000: 114.8,
                25000: 190.7,
                35000: 266.8,
                45000: 343.3,
                55000: 419.6,
                65000: 495.9
            }),
            'purge_percentage': DataSet({
                534: 0.8,
                5534: 8.4,
                10534: 16.1,
                20534: 31.3,
                30534: 46.6,
                40534: 61.9,
                50534: 77.1,
                60534: 92.4,
                65534: 100.0
            }),
            'purge_pressure': DataSet({
                553: 4.3,
                1553: 11.7,
                2553: 19.7,
                3553: 26.8,
                4553: 35.0,
                5553: 42.7,
                6553: 49.6
            }),
            'product_percentage': DataSet({
                534: 0.8,
                5534: 8.4,
                10534: 16.1,
                20534: 31.3,
                30534: 46.6,
                40534: 61.9,
                50534: 77.1,
                60534: 92.4,
                65534: 100.0
            }),
            'product_pressure': DataSet({
                553: 3.7,
                5533: 41.9,
                10553: 80.6,
                20553: 156.7,
                30553: 232.9,
                40553: 309.5,
                50553: 385.6,
                60553: 462.1,
                65534: 500.0
            }),
            'pressure': DataSet({
                553: 34.0,
                5553: 271.1,
                10553: 515.3,
                20553: 1003.6,
                30553: 1491.9,
                40553: 1980.2,
                50553: 2468.4,
                60553: 2956.7,
                65534: 3200.0
            }),
            'level': DataSet({
                534: 0.8,
                5534: 8.4,
                10534: 16.1,
                20534: 31.3,
                30534: 46.6,
                40534: 61.9,
                50534: 77.1,
                60534: 92.4,
                65534: 100.0
            })
        }

    def __getattr__(self, name):
        if name in self.datasets:
            return self.datasets[name]
        raise AttributeError(f"No such dataset: {name}")


def generate_input_values(feed2_perc=None, feed2_press=None, feed1_perc=None, feed1_press=None,
                          purge_perc=None, purge_press=None, product_perc=None, product_press=None,
                          pressure=None, level=None,
                          feed2_perc_spread=0, feed2_press_spread=0,
                          feed1_perc_spread=0, feed1_press_spread=0,
                          purge_perc_spread=0, purge_press_spread=0,
                          product_perc_spread=0, product_press_spread=0,
                          pressure_spread=0, level_spread=0):
    """
    Generates a list of input values based on the provided output values (or defaults to 0 if not provided)
    for each data point: feed2_percentage, feed2_pressure, feed1_percentage, feed1_pressure, purge percentage,
    purge pressure, product percentage, product pressure, pressure, and level.

    Args:
    - Output values for each data point (if not provided, defaults to 0).
    - Spread values for each data point to introduce variability in the input values (if not provided, defaults to 0).

    Returns:
    - A list of input values corresponding to the provided (or default) output values.
    """

    # Initializing the manager
    manager = DataSetManager()

    # Setting default values if not provided
    feed2_perc = feed2_perc if feed2_perc is not None else 0.0
    feed2_press = feed2_press if feed2_press is not None else 0.0
    feed1_perc = feed1_perc if feed1_perc is not None else 0.0
    feed1_press = feed1_press if feed1_press is not None else 0.0
    purge_perc = purge_perc if purge_perc is not None else 0.0
    purge_press = purge_press if purge_press is not None else 0.0
    product_perc = product_perc if product_perc is not None else 0.0
    product_press = product_press if product_press is not None else 0.0
    pressure = pressure if pressure is not None else 0.0
    level = level if level is not None else 0.0

    # Retrieving input values
    results = [manager.feed2_percentage.get_input_value(feed2_perc, feed2_perc_spread),
               manager.feed2_pressure.get_input_value(feed2_press, feed2_press_spread),
               manager.feed1_percentage.get_input_value(feed1_perc, feed1_perc_spread),
               manager.feed1_pressure.get_input_value(feed1_press, feed1_press_spread),
               manager.purge_percentage.get_input_value(purge_perc, purge_perc_spread),
               manager.purge_pressure.get_input_value(purge_press, purge_press_spread),
               manager.product_percentage.get_input_value(product_perc, product_perc_spread),
               manager.product_pressure.get_input_value(product_press, product_press_spread),
               manager.pressure.get_input_value(pressure, pressure_spread),
               manager.level.get_input_value(level, level_spread),
               0, 0, 0]

    return results


# Example usage:

# value_creator = DataSetManager()
# print(value_creator.feed1_percentage.get_input_value(40))
