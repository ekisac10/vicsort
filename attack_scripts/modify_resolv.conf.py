#! /usr/bin/env python3

import os

"""
This script reads the contents of the /etc/resolv.conf file, and ensures that
'nameserver 192.168.90.1' and 'nameserver 8.8.8.8' are present. The modified content is then written back to
/etc/resolv.conf. 
"""

dns_servers = set()
# New DNS server configuration to be added
new_dns_server = 'nameserver 192.168.90.1\nnameserver 8.8.8.8'

# Open /etc/resolv.conf and read its contents
with open('/etc/resolv.conf', 'r') as f:
    lines = f.readlines()

# Loop through each line and look for existing DNS servers
for line in lines:
    if line.startswith('nameserver'):
        dns_servers.add(line.strip())

# Remove any DNS servers that are not 8.8.8.8
for dns_server in dns_servers:
    if dns_server != new_dns_server:
        lines.remove(dns_server + '\n')

# If 8.8.8.8 is not in the file, add it
if new_dns_server + '\n' not in lines:
    lines.append(new_dns_server + '\n')

# Write the modified contents back to /etc/resolv.conf
with open('/etc/resolv.conf', 'w') as f:
    f.writelines(lines)
