# VICSORT - a Virtualised ICS Open-source Research Testbed

This is the primary repository for the VICSORT project.

![banner](figures/custom_banner_1.png)

*Content in the custom VICSORT banner shown at terminal login varies depending on the host computer*.

Testing link to wiki [here](../../wikis/home).


## Table of Contents
- [Overview](#overiew)<br>
- [Network Topology](#network-topology)<br>
- [The Physical Process](#the-physical-process)<br>
- [Testbed in Operation](#testbed-in-operation)<br>
- [Testbed URLs](#testbed-urls)<br>
- [Firewall Configuration](#firewall-configuration)<br>
- [Requirements](#requirements)<br>
- [Installing from scratch](#installing-from-scratch)<br>
- [Prebuilt VM](#prebuilt-vm)<br>
- [VICSORT Contributions](#vicsort-contributions)<br>
- [Credits](#credits)<br>
- [License](#license)<br>


## Overview
VICSORT is a light-weight open-source
ICS testbed, based on [GRFICSv2](https://github.com/Fortiphyd/GRFICSv2#grficsv2), designed to be repeatable, scalable and easy to deploy.  VICSORT leverages LXD containers and KVM to provide a
leaner over build requiring significantly less system resources to operate, compared to its predecessor GRFICS.

VICSORT maintains all the testbed components in [GRFICSv2](https://github.com/Fortiphyd/GRFICSv2#grficsv2) i.e the HMI, 
PLC, engineering workstation, firewall, a physical process simulation and also incoperates an attacker PC based on Kali Linux 2021.

## Network Topology

This testbed assumes the network topology shown below with the following IP address mapping:

|Node       | IP Address Mapping|
| --------- |:-----------------:|
| HMI       | 192.168.90.5 /24 |
| Firewall| - WAN: 192.168.90.100 /24 <br>- LAN: 192.168.95.100 /24
|PLC| 192.168.95.2 /24     
|Engineering Workstation | 192.168.95.5 /24
| Plant Simulation | 192.168.95.10 - 15 /24
|Attacker | 192.168.90.XX /24      

![topology](figures/VICSORT_Topology_1.png)

The graphic below shows how the testbed is testbed can be accessed if your install is a Cloud install.
![cloud](figures/RDP_Setup.png)

## The Physical Process
VICSORT as well as GRFICS are based off the Tennessee Eastman Challenge Process.
The TE plant wide Industrial Control Process problem was
proposed by Downs and Vogel in 1993 as a challenge test
problem to several control related topics such as multivariable
controller design, optimisation, adaptive and predictive control
and non-linear control.

![tennesse](figures/tennessee.png)

The figure above, the gas reactants A, C, D and E enter the
reactor where the reactants undergo an irreversible exothermic
(which means the temperature gradually increases) catalytic
gas phase reaction. Since the reaction is exothermic in nature,
it requires cooling. This function is served by Cold Water
Supply (CWS) and Cold Water Reset (CWR) that control the
water supply to cool the reactor. Inert gas does not undergo
chemical reactions under a set of given conditions. The partial
condenser recovers the products from the reactor exit gas
stream. The stripper is used to minimise the loss of reactants D
and E in the liquid product stream. The gas overhead from the
stripper is combined with the compressed overhead from the
separator and recycled back to the reactor. The purge stream
is used to prevent build-up of excess reactants, the inert B and
the by-product F. The process produces two products from four
reactants. Also present are an inert and a by-product making
eight components A, B, C, D, E, F, G and H. The process
variables are temperature, pressure, level and flow rate.

VISCORT simplifies these process with only two inputs;
Feed1 and Feed2 as well as a Product output and a Purge
byproduct. The process variables measured are pressure (kPa)
and Level (%) and flowrate (kMol/h). The figure below illustrates the
physical process simulation as well as its representation as
shown on the HMI.

![hmi and sim](figures/hmi_and_sim_1.png)

## Testbed in Operation

The figure below shows VICSORT in Operation with the 5 LXD Containers and single KVM VM.

![lxd](figures/lxd_kvm.png)

## Testbed URLs

The following URLs are used when accessing the various components of the testbed.

1. PLC : http://192.168.95.2:8080
2. HMI : http://192.168.90.5:9090/ScadaBR   [username: admin, password: admin]
3. Simulation : http://192.168.95.10
4. Firewall : http://192.168.95.100    [username: admin, password: pfsense]

On the prebuilt VM, these 4 pages will open by default when you launch Google Chrome.

## Firewall Configuration
Pfsense manages communication amongst the nodes in VICSORT.
The nodes that are allowed to communicate with eachother are specified in the firewall rules. Otherwise,
communication is not allowed.

The updated firewall rules in VICSORT are as follows:

### WAN
1. Allow all communication from the HMI to PLC
2. Allow access to the OpenPLC Web User Interface (UI)
from the WAN network
3. Allow access from WAN to simulation VM web interface
4. Allow Internet access for all nodes on the WAN. This
rule is disabled by default and should be enabled when
Internet access is required on the WAN nodes for example for repository updates or further package downloads.
However, the attacker node is exempt from this rule and
always has Internet access.

![wan](figures/wan_firewall_rules.png)

### LAN
1. Allow ICMP to firewall from the 192.168.95.0 /24
(LAN) network. This allows nodes on the LAN to ping
the firewall
2. Allow all communication from the PLC to HMI
3. Allow Internet access to all nodes on the LAN. This rule
is disabled by default and should be enabled when internet access is required on the WAN nodes for example
for repository updates or further package downloads.

![lan](figures/lan_firewall_rules.png)

## Requirements
You can download the vicsort .ova file and import it to VirtualBox to get started.
The following specs are required to run this simulation.

- 4GB RAM
- 30GB HDD
- 2 CPU

## Installing from scratch

1. Follow the setup guide [here](../../wikis/VICSORT-Setup-Guide) to install this testbed from scratch. This setup guide
assumes a clean Ubuntu 20.04 LTS build either in the cloud or in VirtualBox. For Virtualbox, use the [Ubuntu server 20.04 LTS](https://ubuntu.com/download/server) iso. 

## Prebuilt VM

1. Download the vicsort .ova file and import it into VirtualBox. **Note** that this file is about 17GB in size.
   - [VICSORT v1_2021_amd64](https://instituteoftechnol663-my.sharepoint.com/:f:/g/personal/c00265254_itcarlow_ie/EuQU8cIW7bZGp-iUwF9CZLwBZ00auNlo4tKSNYaO53NrWA?e=EufuVq)
   - [VICSORT v2.2_2024_amd64](https://setuo365-my.sharepoint.com/:u:/g/personal/c00265254_setu_ie/EZRaKe3ef6dEgpvgHMGg91QBh5NSbDifg2slOu9PrSoXaw?e=C5Rznc)
2. To launch the testbed after boot, open the terminal and run the initialisation using `sudo python3 Desktop/vicsort/testbed_scripts/testbed_startup.py` 
3. Open *Google Chrome* to view the various web UIs.
4. **Note** that during normal operation, the nodes won't have internet access. Modify the firewall rules to grant them internet access.

> Note that if you notice that the simulation-container doesn't have 5 IP addresses assigned to it after running the script, run it again till you see 5 IP addresses on the simulation-container.


## VICSORT Contributions
VICSORT makes the following contributions to ICS cybersecurity:

- An all-in-one open-source ICS testbed build; rebuilt with LXD and KVM to provide a leaner overall build requiring
significantly less system resources to operate. 
- The testbed is easily deployable on a Type 2 hypervisor or on the Cloud.
- A detailed guide on how to reproduce the testbed from
scratch to provide a more in-depth understanding to how
the ICS components operate.
-  A number of modifications to the testbed components
including python library upgrades/replacements to the
currently outdated and publicly unavailable libraries used
in the GRFICS version, Operating System (OS) upgrades,
firewall upgrades to more closely emulate a real-world ICS,
as well as architectural and design modifications.

## Credits
This builds on the [GRFICSv2](https://github.com/Fortiphyd/GRFICSv2) build by @djformby.

## Licence
Unless otherwise specified, everything in this repository is covered by the following licence:

- VICSORT is licenced under the [European Union Public Licence v1.2 (EUPLv1.2)](https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_en.pdf).

<br><br>
******
Version 1.2, Feb 2024

By Conrad Ekisa
******
