#! /bin/python3

# This script is intended to start and check that OpenPLC is running
# on the PLC Container.

# Assumes OpenPLC is installed in the container /root dir.

import os
from time import sleep

openplc_status = os.popen("ps aux | grep openplc").read().split("\n")
check_openplc = False

nodejs_status = os.popen("ps aux | grep nodejs").read().split("\n")
check_nodejs = False

for item in openplc_status:
    if "core/openplc" in item:
        check_openplc = True

for item in nodejs_status:
    if "nodejs server.js" in item:
        check_nodejs = True

def start_openplc():
    os.system("cd /root/OpenPLC* && nodejs server.js &")

if check_nodejs == True and check_openplc == True:
    print("OpenPLC is running\n")
    os.system("netstat -lntp")
else:
    print("OpenPLC is not running. Starting OpenPLC now...\n")
    start_openplc()
    sleep(1)
    os.system("netstat -lntp")
    print("\nSuccessfully Started OpenPLC\n")

