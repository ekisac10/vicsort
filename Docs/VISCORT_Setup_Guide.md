# VICSORT Setup Guide

VICSORT - a **Virtualised ICS Open-source Research Testbed** is a modified build of the [GRFICSv2](https://github.com/Fortiphyd/GRFICSv2) testbed thats designed to be lighter-weight, easier to deploy and initialise.
This guide shall walk you through the process of setting up this testbed from scratch, should you be interested in doing so.

This guide assumes that you have a clean [Ubuntu Server 20.04 LTS](https://releases.ubuntu.com/20.04.3/ubuntu-20.04.3-live-server-amd64.iso) install either in **Virtualbox** or in the **cloud**.
This guide shall walk you through the setup process, from the clean VM install through to the complete VICSORT installation process.

> Note that some sessions document errors I encountered whilst setting up the testbed and how I resolved them. Skip these sections
if you don't encounter similar errors.

Well, let's get started !!!

## Table of Contents
- [The Testbed Network Topology](#the-testbed-network-topology)<br>
- [Preparing the Host Machine](#preparing-the-host-machine)<br>
  - [Setup SSH](#setup-ssh)<br>
  - [Install Packges and Additional Files](#install-packages-and-additional-files)<br>
  - [Setting Up RDP for Cloud Access](#setting-up-rdp-for-cloud-access)<br>
- [Setup the LXD Container Environment](#setting-up-the-lxd-containers)<br>
  - [Creating LXD Network Bridges](#creating-lxd-network-bridges)<br>
  - [Creating LXD Profiles](#creating-lxd-profiles)<br>
- [HMI Container](#hmi-container)<br>
  - [Configuring HMI Container](#configuring-hmi-container)<br>
- [Attacker Container](#attacker-container)<br>
  - [Access Attacker Container GUI](#access-attacker-container-gui)<br>
- [PfSense Firewall](#pfsense-firewall)<br>
  - [Dealing with XDisplay Issues](#dealing-with-xdisplay-issues)<br>
  - [Deleting a VM](#deleting-a-vm)<br>
  - [PfSense Network Adapters](#pfsense-network-adpaters)<br>
  - [Configuring PfSense](#configuring-pfsense)<br>
  - [Configuring PfSense Web Configurator](#configuring-pfsense-web-configurator)<br>
- [PLC Container](#plc-container)<br>
  - [Provisioning PLC Container](#provisioning-plc-container)<br>
  - [Setting up OpenPLC from zip](#setting-up-openplc-from-zip)<br>
  - [OpenPLCv2 Upload Bug](#openplc-v2-upload-bug)<br>
  - [Fix to Upload Bug](#fix-to-upload-bug)<br>
- [Simulation Container](#simulation-container)<br>
  - [Setting up Container](#setting-up-container)<br>
  - [Provisioning Simulation Container](#provisioning-simulation-container)<br>
  - [Setting Up the Web Visualisation](#setting-up-the-web-visualisation)<br>
  - [Challenges with Unity Build](#challenges-with-unity-build)
- [Workstation Container](#workstation-container)<br>
  - [X Display Errors](#x-display-errors)<br> 
- [VICSORT Initialisation Script](#vicsort-initialisation-script)<br>
  - [Installing required python packages](#installing-required-python-packages)<br>
  - [Running the Initialisation Script](#running-the-initialisation-script)<br>
  - [What does this script do?](#what-does-this-script-do)<br>
- [Installing Custom VICSORT modt](#installing-custom-vicsort-motd)<br>
  - [Setting up the Custom banner](#setting-up-the-custom-banner)<br>
- [Restarting VICSORT](#restarting-vicsort)<br> 
    

  
## The Testbed Network Topology

Below is the network topology used in VICSORT. This topology will be referred to in the coming sections.

![topology](../figures/VICSORT_Topology_1.png)

This testbed is designed to be a self-contained testbed. This means that any URLS used in the testbed, for example the
HMI URl or URL to the simulation are intended to be accessed within the Host Machine (in the VM or Cloud). Its possible to
access them outside though that will not be covered in this guide.

However, since the Host Machine is Server based, i.e has no GUI, a GUI must be installed on the Host Machine.
As shown in the diagram below, to access the GUI aspect of the testbed, a user will need to RDP (Remote Desktop Protocol) from their machine to the
Host Machine (machine hosting the VISCORT Testbed). Within this session, the user will be able to use the web browser to access
the web portal utilities that most of the components leverage for their operation. The PLC, Firewall, HMI and Simulation all
have URLs for management.

Furthermore, the attacker container within the DMZ will also be accessed via RDP as its envisaged that the attacks will
be initiated from the attacker container and not the Host Machine.


![rdp](../figures/RDP_Setup.png)

Therefore a Desktop must be installed on the Host Machine as well as an RDP server to facilitate RDP. For this install,
`lubuntu-desktop` is used. With `lubuntu-desktop` set up, if you are using VirtualBox, you will not need RDP as Vbox provides a virtual
console. However, if your install is in the cloud, then you will need to use RDP as specified above.


## Preparing the Host Machine


### Setup SSH

I prefer to SSH into the machine whether its in VirtualBox or in the Cloud so this shall be the first step. The assumption at this stage is that you are able to reach the Host Machine via its IP, either on the LAN
through a bridged adapter - for VirtualBox installs, or through a public IP - for Cloud installs.

Through the installation process, you will be working as root, unless stated otherwise.
~~~bash
apt update && apt upgrade

apt install -y openssh-server

# Create vicsort user and add the user to the sudoers list
# Set the password to "vicsort" or something you can remember
adduser vicsort
adduser vicsort sudo

# Run this as viscort user
ssh-keygen -C "vicsort"

# Modify SSH settings
# Uncomment the line below
nano /etc/ssh/sshd_config
  PasswordAuthentication yes

# Restart SSH
service sshd restart
~~~

At this stage, you should be able to successfully SSH into the Host Machine. Copy your machine's public ssh key as follows:

~~~~bash
ssh-copy-id vicsort@{ip_address}

# You can now SSH using your public key
ssh vicsort@{ip_address}
~~~~

#### Using .pem files

If you have a .pem file from AWS and wish to SSH to your machine using this .pem file. On your host machine, simply:

~~~bash
# On your host machine
ssh-add your_pem_file.pem

ssh ubuntu@amazon_ip_address
~~~~

### Install Packages and Additional Files

Additional files and packages are required before the installation process can begin.

~~~~~bash
at update && apt upgrade -y

# This may take a while.
apt install -y xrdp lxd net-tools git lubuntu-desktop freerdp2-x11 chromium-browser python2 python3-pip

# Removes the notorious flurry process that eats up alot of CPU
 apt-get remove xscreensaver-gl-extra -y
~~~~~

You must restart the Host Machine to start the GUI. In Vbox, the GUI should pop up upon the next restart.
Just before you enter the password for the **vicsort** user account, in the bottom right corner, click the settings
icon and select the **lubuntu** desktop environment, then proceed to enter your password and login.

#### [Optional]: Installing VirtualBox Guest Additions

This process allows the VM and host computer to share the clipboard as well as allows the user to adjust the **virtual screen size**.
This can be handy to make working with the VM easier. When copying commands from the Linux
terminal in the VM, highlight the commands or content you wish to copy and use the keyboard shortcut **Ctrl + Shift + C** to copy the content. Then
a simple **Ctrl + V** to paste the commands in a test editor anywhere else. To paste commands into the Linux terminal, use the keyboard shortcut **Ctrl + Shift + V**.


To install VirtualBox Guest Additions, when the VM boots, on the top *Menu Bar >> Devices >> Insert Guest Additions CD Image*. This shall mount the image
onto your VM. If you dont have VBox Guest Additions downloaded on your host, VirtualBox shall prompt you to allow it download this file on your behalf.
Open a terminal and do the following(This step assumes that the VM has a working internet connection):

~~~~bash
# You shall need to press the Enter key at a certain point in the installation
sudo apt-get install -y virtualbox-guest-dkms virtualbox-guest-utils
Sudo apt install -y virtualbox-guest-x11

# Usually removable media is mounted under /media/your_current_user/
cd /media/ubuntu/VBox_GA_5.2.42
sudo ./VboxLinuxAdditions.run
sudo ./autorun.sh
~~~~

You can reboot the VM just to be sure its installed successfully.

In the top menu bar, select **View > Virtual Screen 1 > Resize to 1024x728**. After this, use the command `reboot` to reboot the VM.
Once this process is complete, you should have a larger window size for your VM. This also indicates that the process worked successfully.

### Continue Installation Process ...

~~~~bash
su vicsort
cd /home/vicsort
git clone https://gitlab.com/ekisac10/vicsort.git
~~~~


Below is the `lxd init` configuration.

~~~bash
root@vicsort:/home/vicsort# lxd init


Would you like to use LXD clustering? (yes/no) [default=no]:

Do you want to configure a new storage pool? (yes/no) [default=yes]:

Name of the new storage pool [default=default]: ics_default_pool

Name of the storage backend to use (zfs, ceph, btrfs, dir, lvm) [default=zfs]:

Create a new ZFS pool? (yes/no) [default=yes]:

Would you like to use an existing empty block device (e.g. a disk or partition)? (yes/no) [default=no]:

Size in GB of the new loop device (1GB minimum) [default=5GB]: 30

Would you like to connect to a MAAS server? (yes/no) [default=no]:

Would you like to create a new local network bridge? (yes/no) [default=yes]:

What should the new bridge be called? [default=lxdbr0]:

What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: 172.16.10.1/24

Would you like LXD to NAT IPv4 traffic on your bridge? [default=yes]:

What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: none

Would you like the LXD server to be available over the network? (yes/no) [default=no]: no

Would you like stale cached images to be updated automatically? (yes/no) [default=yes] yes

Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]: no

~~~~


### Setting UP RDP for Cloud Access

RDP should work automatically, or maybe after a `service xrdp restart`. If this doesn't work, refer to the subsection below.

#### (Not Used) Using xfce Panel
This install doesn't use xfce panel, and uses lubuntu-desktop instead. However, if for some reason you are getting
issues with RDPing (getting RDP access) to your machines like x11 Display issues, then use xfce.

~~~bash
apt install xfce4

service xrdp stop

# Comment out the lines as below and add the following command below
nano /etc/xrdp/startwm.sh
  #test -x /etc/X11/Xsession && exec /etc/X11/Xsession
  #exec /bin/sh /etc/X11/Xsession
  startxfce4

service xrdp restart

~~~~~

This should get RDP working on the Host Machine.

#### RDP from Linux Machine

To RDP from a Linux Machine, there are 2 options:

1. <p> You can use the **xfreerdp** utility thats installed with `apt install freerdp2-x11`. TO RDP into the machine,
   use the command `xfreerdp /v:{ip_address} /u:vicsort`. This will pop up another window where you will be prompted to enter your password.
   </p>
2. You can use the **Remmina** utility. Simply provide the ip_address, username and password and you are good to go. Remember the credentials are **Username**: vicsort, **Password**: vicsort


#### RDP From Windows Machine

To RDP From a Windows Machine, simply launch the **Remote Desktop Connection** and provide the Host Machine ip_address.
You will then be prompted for the username and password and get access to the Host Machine.

#### Disabling GUI (Not Required for Cloud Installs)
This is not required if you would like to have a GUI on the host machine. However, I chose to disable the GUI on the Host Machine and as such
document that here. The GUI is still accessible via RDP.
Disabling the GUI makes the boot process faster and the machine generally faster when using the CLI
This [guide](https://linuxconfig.org/how-to-disable-enable-gui-on-boot-in-ubuntu-20-04-focal-fossa-linux-desktop) shows how to do this. It
also shows how you can enable the GUI if need be.

~~~~~bash
sudo systemctl set-default multi-user

gnome-session-quit

reboot
~~~~~


## Setting up the LXD Container Envrironment


### Creating LXD Network Bridges

From the network topology in Section [The Testbed Network Topology], there are 2 networks used, the **192.168.90.0/24** and **192.168.95.0/24** networks. We need to create
those networks for the LXC respective containers to eventually attach to. In order to do this, I create a 2 bridge interfaces namely: **dmzzone** and **icszone**.
Note that `lxd init` created a bridge interface called **lxdbr0**. Let's ignore this bridge for now. To create the above mentioned bridges:

~~~bash
lxc network create icszone ipv6.address=none ipv4.address=192.168.95.1/24 ipv4.nat=true
lxc network create dmzzone ipv6.address=none ipv4.address=192.168.90.1/24 ipv4.nat=true

root@vicsort:/home/vicsort# lxc network list
+---------+----------+---------+-------------+---------+
|  NAME   |   TYPE   | MANAGED | DESCRIPTION | USED BY |
+---------+----------+---------+-------------+---------+
| dmzzone | bridge   | YES     |             | 1       |
+---------+----------+---------+-------------+---------+
| enp0s3  | physical | NO      |             | 0       |
+---------+----------+---------+-------------+---------+
| icszone | bridge   | YES     |             | 1       |
+---------+----------+---------+-------------+---------+
| lxdbr0  | bridge   | YES     |             | 1       |
+---------+----------+---------+-------------+---------+
~~~~

### Creating LXD Profiles

LXD Profiles make it easy to provision settings that apply to a particular section of containers within that profile. The created LXD profiles will associate
the created network bridges to the create containers. Two profiles are created: **dmzzone-profile** and **icszone-profile**.

~~~~bash
lxc profile create dmzzone-profile
lxc profile create icszone-profile

# This opens up either a nano or vim editor. You may need to know how to use vim when creating this profile
# Press "i" to edit. After editing, press "ESC" and ":q" to quit, ":wq" to save and quit
# You can delete all the contents and simply copy and paste the lines below

lxc profile edit dmzzone-profile
config: {}
description: "DMZ Zone LXD Profile"
devices:
   eth1:
     name: eth1
     network: dmzzone
     type: nic
   root:
      path: /
      pool: ics_default_pool
      type: disk
name: dmzzone-profile
used_by:


lxc profile edit icszone-profile
config: {}
description: "ICS Zone LXD Profile"
devices:
   eth1:
     name: eth1
     network: icszone
     type: nic
   root:
     path: /
     pool: ics_default_pool
     type: disk
name: icszone-profile
used_by:

root@vicsort:/home/vicsort# lxc profile list
+-----------------+---------+
|      NAME       | USED BY |
+-----------------+---------+
| default         | 0       |
+-----------------+---------+
| dmzzone-profile | 0       |
+-----------------+---------+
| icszone-profile | 0       |
~~~~~


## HMI Container


The HMI-container is in the DMZ zone and shall be attached to the **192.168.90.0/24** network.

~~~~bash
# To launch the container
lxc launch images:ubuntu/focal/amd64 hmi-container -p dmzzone-profile

~~~~

To assign a static IP of **192.168.90.5** to the container:

~~~~bash
lxc stop hmi-container
lxc network attach dmzzone hmi-container eth1 eth1
lxc config device set hmi-container eth1 ipv4.address 192.168.90.5
lxc start hmi-container
lxc exec hmi-container bash
dhclient -v
~~~~

At this stage, you should be able to `ping google.com`, i.e have internet on the container as well as have your static IP on the corresponding network.
Later on, internet access on this container will be managed via the firewall.

### Configuring HMI-Container

Log into the HMI-Container using `lxc exec hmi-container bash`.

~~~~~~bash
apt install unzip wget nano git net-tools -y

git clone https://github.com/thiagoralves/ScadaBR_Installer.git
cd ScadaBR_Installer
./install_scadabr.sh
~~~~~~

>The following files below need to be downloaded to the Host machine and **NOT** the HMI-Container as they shall be uploaded via the ScadaBR web interface as we set up the HMI.

~~~~~bash
exit
root@ip:/home/vicsort: mkdir hmi_files && chown -R vicsort:vicsort hmi_files/ && cd hmi_files
wget https://github.com/Fortiphyd/GRFICSv2/raw/master/hmi_vm/ChemicalPlantScadaBR.zip
unzip ChemicalPlantScadaBR.zip -d ChemicalPlantScadaBR
sudo chown -R  vicsort:vicsort *
cd ChemicalPlantScadaBR

~~~~~

At this stage, you should RDP into the Host Machine (for Cloud installs) or access the Host Machine's GUI (for Virtual Box) installs.
If you restarted the Host Machine, you need to log into the **hmi-container**, obtain an IP address with `dhclient -v`, and run the command `/opt/tomcat6/apache-tomcat-6.0.53/bin/startup.sh` to launch ScadaBR.
ScadaBR can then be accessed via **http://192.168.90.5:9090/ScadaBR**.

The default login credentials are username: **admin**, password: **admin**. You will be greeted with a Welcome Message.
Hover over the icons in the top taskbar, find and click the **import/export** icon. Under **Import Project**, Navigate to `hmi_files` and select the **zip folder** called *ChemicalPlantScadaBR.zip*
(ScadaBR can import a zip file and extract its contents). Hit **Send >> Import >> OK**. The system will restart shortly after.

You may need to upload the HMI background image. Within the ScadaBR web UI, this can be done from **Graphical Views** Icon. **Graphical Views >> Edit View >>
Background Image >> Browse >> `ChemicalPlantScadaBR/uploads/4.png` >> Upload Image.**. Click **Save** at the bottom of the page.

There should be Caution signs on all the data points. This is because the PLC and Simulation Containers are not setup yet.
Once they are correctly setup, the data point should automatically come in.


You will need to navigate to **Data Sources >> Edit >> Host** and change this to **192.168.95.2**.
At the top right of the text area, hit the **save** icon and navigate back to **Graphical Views**.
You should see data coming through now, if the PLC has been setup and is running at this stage.


## Attacker Container


The attacker PC shall be a Kali Linux PC attached to the DMZ network, i.e to the 192.168.90.0/24 network. A good guide to setting up [Kali on LXD](https://www.kali.org/docs/containers/kalilinux-lxc-images/).
This particular Kali Container image will be setup with a GUI as below.

~~~bash
cd /home/vicsort/Downloads

# This profile must be created and applied to the gui-kali  whilst its being provisioned.
wget https://blog.simos.info/wp-content/uploads/2018/06/lxdguiprofile.txt

# Modify lxdguiprofile.txt
# Your modified lxdguiprofile.txt file should read as below
nano lxdguiprofile.txt
config:
  environment.DISPLAY: :0
  raw.idmap: both 1000 1000
  user.user-data: |
	#cloud-config
	runcmd:
	  - 'sed -i "s/; enable-shm = yes/enable-shm = no/g" /etc/pulse/client.conf'
	  - 'echo export PULSE_SERVER=unix:/tmp/.pulse-native | tee --append /home/ubuntu/.profile'
	packages:
	  - x11-apps
	  - mesa-utils
	  - pulseaudio
description: GUI LXD profile
devices:
  X1:
	bind: container
	connect: unix:@/tmp/.X11-unix/X1
	listen: unix:@/tmp/.X11-unix/X1
	type: proxy
  mygpu:
	type: gpu
name: gui-kali
used_by:


lxc profile create gui-kali
cat lxdguiprofile.txt | lxc profile edit gui-kali
lxc profile list

lxc launch images:kali/current/amd64 attacker-container -p dmzzone-profile -p gui-kali

# A simple script to speed up the provisioning process. Feel free to modify it as you wish.
root@vicsort:/home/vicsort/vicsort/testbed_scripts# python3 provision_lxd.py

# Console into the gui-kali
lxc exec attacker-container  bash

apt update && apt upgrade -y
adduser kali

# Username: Kali, Password: Kali
usermod -aG sudo kali

dhclient -v
apt install -y kali-linux-default xfce4-panel xrdp kali-desktop-xfce

# You will get afew prompts as you go through this process. Use the defaults provided, unless otherwise.
~~~~~~


### Access Attacker-Container GUI
To access the attacker-container GUI, we shall RDP from the Host Machine into the attacker-container as shown in
Figure [Accessing the GUI Components of the Testbed].

~~~bash
lxc exec attacker-container bash

dhclient -v
systemctl enable xrdp
service xrdp start
~~~~~

From the host machine, RDP to the attacker-container with the command `xfreerdp /v:{attacker_ip_address} /u:kali`.


## PfSense Firewall

The pfsense firewall shall be attached to two networks i.e the *icszone* and the *dmzzone*. PfSense cant be run on
LXD yet because PfSense is based on FreeBSD which is currently not supported on LXD. As such, we will use KVM to run
PfSense directly from its iso.

~~~~bash
# On the host machine

apt install qemu-kvm virt-manager -y
systemctl enable --now libvirtd

# Download PfSense iso
chown -R vicsort:vicsort /home/vicsort/Downloads
cd /home/vicsort/Downloads
wget https://frafiles.netgate.com/mirror/downloads/pfSense-CE-2.5.1-RELEASE-amd64.iso.gz
gunzip pfSense*

root@vicsort:/home/vicsort# umount -f thinclient_drives
# If you get an error at this step, ignore and move on

~~~~~


>This step must be run on a GUI (either via Virtual Box or RDP). You **MUST** run the commands below as the `vicsort` user.

~~~~~
# Run this command as vicsort user. If it returns an error, move on the next step
xhost si:localuser:root

# Install pfSense
cd /home/viscort/Downloads
~~~~~~

At this stage, I run into **XDISPLAY: cannot open display** issues. Refer to subsection [Dealing with XDisplay Issues] if you run into similar challenges.


~~~~~
# I recommend creating this VM as root as it makes modifying it much easier later on
sudo virt-install --name=pfsense-vm --vcpus=1 --memory=512 --cdrom=pfSense-CE-2.5.1-RELEASE-amd64.iso --disk size=5 --os-variant=freebsd8.0

~~~~~

This should pop up a `virt-viewer` window where you should be able to manage the installation from. Use the default
settings provided. No fancy configurations are made during the installation process. Pfsense should reboot after the install is complete.


To access the **pfsense-vm** console,

~~~bash
# Run this command as vicsort user
vicsort@vicsort:~/Downloads$ sudo virt-viewer pfsense-vm
~~~~

Otherwise, you should still have a **virt-viewer** window open and after the reboot, pFsense will require some configuration.
Hold that thought, close **virt-viewer** and proceed to shutdown the VM then move to subsection [PfSense Network Adpaters].

Further operations regarding the VM like Starting the VM, shutting it down **MUST** be run with the user that created the VM, in this case, thats the
*vicsort* user. Shutdown the VM using `virsh shutdown pfsense-vm`.

### Dealing with XDisplay Issues
During this install process, I kept running into xdisplay issues similar to [those described here](https://www.linuxquestions.org/questions/linux-software-2/cannot-open-display-as-root-352200/).
Specifically, `virt-viewer` requires xdisplay to be able to open up, as it required a Display interface (GUI) which is managed by XDisplay.
This seemed to work fine with the non-root **vicsort** user but didn't work fine with **root**. **root** couldn't open up virt-viewer and returned an *cannot open display* error.

My solution to this was 2 fold. First you check which DISPLAY is being used as shown in subsection [X Display Errors], and ensure that its set to the same DISPLAY
as on the **vicsort** user, then set XAuthority on the non-root user. In my case, `DISPLAY=:10` on the vicsort user.

Note that this approach is only temporary and doesn't persist reboot. For this change to persist reboot. you need to add the `export XAUTHORITY` to `~/.bashrc` for the vicsort uservicsort
. Refer to subsection [X Display Errors] for how to do this.

~~~~~bash
# Run as vicsort user
export XAUTHORITY=/home/vicsort/.Xauthority
# Confirm DISPLAY environment variable value
echo $DISPLAY

# Run as root
echo $DISPLAY # This should be the same as on the vicsort user
echo $XAUTHORITY # This should be the same as on the vicsort user

# Test that its working by running
virt-viewer

# This should pop up either a virt-viewer window if a VM is running or an error if no VM is running.
~~~~~~

### Deleting a VM
Incase you need to delete and recreate the VM, use the commands

~~~~bash
virsh shutdown pfsense-vm
virsh destroy pfsense-vm 2> /dev/null
virsh undefine  pfsense-vm
virsh pool-refresh default

# If this returns an error, then its all good
virsh vol-delete --pool default pfsense-vm.qcow2
~~~~
This is a [kvm cheatsheet](https://blog.programster.org/kvm-cheatsheet) to get you familiar with kvm commands as well a list of all the supported [kvm OS variants](http://thomasmullaly.com/2014/11/16/the-list-of-os-variants-in-kvm/).


### PfSense Network Adpaters

You need to assign adapters to pfSense. In this case, we would like to attach the **dmzzone** and **icszone** adapters to the VM.
The WAN interface is on the DMZ subnet (192.168.90.0/24) at 192.168.90.100 and the LAN interface is on the ICS subnet (192.168.95.0/24) at 192.168.95.100.
The VM **doesn't** need to be shutdown for this operation.

IF the VM is off, start it with `virsh start pfsense-vm`.

~~~bash
# Run as root
virsh attach-interface --domain pfsense-vm --type bridge --source dmzzone --model virtio --config --live
virsh attach-interface --domain pfsense-vm --type bridge --source icszone --model virtio --config --live
virsh domiflist pfsense-vm

# There should only be two network sources i.e. dmzzone and icszone. If the default source exisits
# Then take note of the mac address and detach the network interface
virsh detach-interface --domain pfsense-vm --type network --mac 52:54:00:8b:8a:1d
~~~~~


### Configuring PfSense

At this stage, you need to setup the WAN and LAN interfaces of the firewall.
Reboot the VM before proceeding with the configuration using `virsh reboot pfsense-vm`. You can then access it with `virt-viewer pfsense-vm`.
There shall be a series of questions
on the PfSense console. Answer them as below:

~~~~bash
Should VLANs be set up now? n

# The WAN interface is on the dmzzone network so from the 'virsh domiflist pfsense-vm' command, identify the correct adapter
# The interface numbering may be different but the hierarchical order is maintained.
Enter the WAN interface name or 'a' for auto-detection (vtnet0 vtnet1 or a): vtnet0

Enter the LAN interface name of 'a' for auto-detection: vtnet1

Do you want to proceed? y

~~~~

PfSense shall then go ahead and set up the adapters, the firewall and web portal.
You will be greeted with the default PfSense console. There are afew more configurations to be made.
The WAN and LAN interfaces need to be assigned static IP addresses and gateways.

To configure the static IP addresses and gateway. It may be neccessary to configure the WAN and LAN interface
one at a time. Always try to ping the gateways with the **Ping Host** after making the static IP address configuration.

- WAN Interface: 192.168.90.100/24, Gateway: 192.168.90.1 (IP address of the dmzzone bridge facing the Host Machine)
- LAN Interface: 192.168.95.100/24, Gateway: 192.168.95.1 (IP address of the icszone bridge facing the Host Machine)

~~~~bash
# Configure WAN
Enter an option: 2

# 1 is the WAN, 2 is the LAN
Enter the number of the interface you wish to configure: 1

Configure the new WAN IPv4 interface via DHCP?: n

Enter the new WAN IPv4 address. Press ENTER for none: 192.168.90.100/24

For a WAN, enter the new WAN IPv4 upstream gateway address: 192.168.90.1

Configure IPv6 address WAN interface via DHCP6? n

Enter the new WAN IPv6 address. Press ENTER for none: ENTER

Do you wan to revert to HTTP as the webConfigurator protocol? y

Press ENTER to continue: ENTER


# Configure LAN
Enter an option: 2

Enter the number of the interface you wish to configure: 2

Enter the new LAN IPv4 address. Press ENTER for none: 192.168.95.100/24

For a WAN, enter the new LAN IPv4 upstream gateway address: 192.168.95.1

Enter the new LAN IPv6 address. Press ENTER for none:

Do you want to enable the DHCP server on the LAN? n
~~~~

You should now be able to log into pfSense via the web browser using the LAN IP at http://192.168.95.100

You can also SSH into the pfSense Vm using the command. For this, you require the followins:

- You must have enabled SSH. You can do this using `virt-viewer`
- The username and password are the credentials set for the Web UI.
- Use the LAN IP to SSH.

You can then ssh using `ssh admin@192.168.95.100`, with the password of *pfsense* (or whichever password you set).

### Configuring PfSense Web Configurator

On your host web browser, access the pfSense Console via the web interface using the default credentials: **admin**, **pfsense**.
You shall then go through the initial configuration setup. You may alter these configs to your liking, However, I maintained most of the defaults.
My configs were as follows (If a configuration isn't listed below, it was left with its default value):

- Hostname: pfSense
- Domain: icstestbed.org
- Primary DNS Server: 8.8.8.8
- New Admin Password: pfsense


A configuration file is already available, and has been custom built for this VICSORT Testbed. This *base_firewall_configs*
file is available in the **/home/vicsort/vicsort/setup_files/** dir and should be imported into pfSense.

In the pfsense web interface, navigate to **Diagnostics >> Backup & Restore >> Restore Backup >> {Provide configuration file} >> Restore Configuration**
The firewall shall reboot. You shall need to re-specify the WAN(192.168.90.100/24) and LAN(192.168.95.100/24) IP addresses if the new configuration causes them to change.
Follow the pfsense prompts to do this. PfSense will reboot to take the new configurations. When you are done, the credentials to log back into the pfSense web browser are **admin**, **pfsense**.
You will notice new rules under **Firewall >> Rules**.


A few notes regarding the base configurations in this testbed.

- <p>There are no host based firewalls implemented. Therefore all communication within a particular LAN is allowed. In this case, all nodes within the LAN / ICS Zone
  can communicate with eachother and all nodes within the WAN / DMZ Zone can communicate with each other.</p>
- Communication between the WAN and LAN is all via the firewall. All the nodes in this setup have their default routes pointing to the firewall to have all traffic checked against the firewall rules below.
- Only the PLC in the ICS Zone is allowed to communicate out to the HMI via the firewall. All other nodes within the ICS zone cant communicate out to the WAN.
- There are disabled rules to allow access tot he internet for both the WAN and LAN. These rules can be enabled incase packages need to be installed on the nodes within the LAN or WAN.

<br>

![WAN_Firewall_Rules](../figures/wan_firewall_rules.png)

<br>

![LAN_Firewall_Rules](../figures/lan_firewall_rules.png)


## PLC Container

The PLC container shall be attached to the ICSZONE - 192.168.95.0/24 network with an IP address of **192.168.95.2/24**.
The install guide for this step can be found [here]("https://github.com/Fortiphyd/GRFICSv2/tree/master/plc_vm"). I found that the best way to get OpenPLC to work
was to copy a working copy of OpenPLC from the original GRFICSv2 and import it to the containers. Installing it manually and running it just didnt seem to work well. The communication
between the HMI and the Simulation container wasn't working right.


### Provisioning plc-container
~~~bash
lxc launch images:ubuntu/focal/amd64 plc-container -p icszone-profile

# This is a custom script I wrote to help with the initial container setup.
python3 provision_lxd.py

lxc stop plc-container
lxc network attach icszone plc-container eth1 eth1
lxc config device set plc-container eth1 ipv4.address 192.168.95.2
lxc start plc-container && lxc exec plc-container -- dhclient -v
~~~~

!!! Warning
   If in the above stage, the **plc-container** is still obtaining an IP address via DHCP as oppossed to the static IP address of 192.168.95.2,
   then you may need to reboot the host machine.

### Setting up OpenPLC from zip
There is a **openplc2** zip file in the **/home/vicsort/vicsort/setup_files/** dir.

`````bash

cd /home/vicsort/vicsort/setup_files/
lxc file push openplc2.zip plc-container/root/
lxc file push provision_plc.py plc-container/root/

lxc exec plc-container bash
apt install unzip nodejs -y
chmod +x provision_plc.py
unzip openplc*
cd OpenPLC_v2
apt-get install build-essential pkg-config bison flex autoconf automake libtool make nodejs git -y

# You may be prompted to add DNP3 support. Select N for this.
# When prompted, use the Modbus driver
./build.sh


# If there were no errors, test to see if openplc server launches
nodejs server.js &

# If it launches successfully (Working on port 8080), then kill the server for further configuration.
`````
OpenPLC on the plc-continer can be started with `python3 provision_plc.py`.
You can then access the PLC via **http://192.168.95.2:8080**.
The PLC should be default be running the *Simplified_TE.st* file.
You can download and upload either file via the links below. Download this files to the Host machine and not the plc-container.

```bash
cd ~ && mkdir -p plc_files && cd plc_files

wget -L --no-check-certificate https://raw.githubusercontent.com/Fortiphyd/GRFICSv2/master/workstation_vm/attack.st
wget -L --no-check-certificate https://raw.githubusercontent.com/Fortiphyd/GRFICSv2/master/workstation_vm/simplified_te.st
`````

There is no need to upload the files at this stage. I also notice here that whenever an upload happens,  this causes an error with OpenPLC causing it to terminate and need restarting.
Luckily, there is no need to upload a file yet.

### OpenPLC v2 Upload Bug
As earlier mentioned, there was a bug in the upload feature in this OpenPLCv2 install. I am not sure if this bug appears on all OpenPLCv2 installs.
However, this is how the bug manifests itself. It causes OpenPLC to crash and the program needs to be manually restarted to get it working again.
Below shows what appears in the plc-container console just before the program crashes. It points to an error during the compiling of the new *.st* file, in this case, *attack.st*.

![openplc_bug_console](../figures/openplc_bug_console.png)

On the web interface, the web server simply becoems unavailable as it has crashed in the backend.

![openplc_bug_web](../figures/openplc_bug_web.png)

On investigation, this pointed to the `iec2c` application thats called to compile any new PLC files that have been uploaded. the `iec2c` application was corrupted and simply couldnt run.

As a fix, I compiled OpenPLCv3, which happened to use the same `iec2c` file and simply replaced this corrupted `iec2c`
file in the OpenPLCv2 dir with the working `iec2c` file from OpenPLCv3.
You should now be able to upload new PLC files with no problem.

This is particularly useful when uploading the `attack.st` file that simulated a changed PLC value that causes damage to the physical process.

### Fix to Upload Bug

A working copy of the `iec2c` file is stored in the `vicsort/setup_files/OpenPLC_files` dir.

~~~~bash
lxc exec plc-container bash
cd OpenPLC_v2
mv iec2c iec2c.bak
exit

# Navigate to your vicsort install
cd ~/vicsort/setup_files/OpenPLC_files
lxc file push iec2c plc-container/root/OpenPLC_v2/

~~~~~

Incase OpenPLC isn't active yet, you can start it using the `provision_plc.py` script within the **plc-container**.


## Simulation Container

The simulation VM (named ChemicalPlant) runs a realistic simulation of a chemical process reaction that is
controlled and monitored by simulated remote IO devices through a simple JSON API. These remote IO devices are then
monitored and controlled by the PLC VM using the Modbus protocol. This VM is located in the ICS network (192.168.95.0/24)
with the IP addresses 192.168.95.10-192.168.95.15.

As it happens, this simulation setup was built with currently deprecated python libraries. I tried to build the VM using updated python3 pymodbus
packages but did not have success in having the simulation correctly communicate with the PLC and thus the HMI. Therefore I picked the original python2
pymodbus libraries from the original Simulation VM (Theses are now available in the **/home/vicsort/vicsort/setup_files/** dir) and import them into the *simulation-container*
build.

### Setting up Container

This bash command breakdown is in grouping. The commands should be copied and pasted in

~~~~~bash
# Install Ubuntu 16 on this container
lxc launch images:ubuntu/xenial/amd64 simulation-container -p icszone-profile

# Copy and paste these commands in the corresponding stages i.e stage 1
lxc stop simulation-container
# This deletes interface eth1
lxc config device add simulation-container eth1 none
lxc network attach icszone simulation-container eth2 eth2
lxc config device set simulation-container eth2 ipv4.address 192.168.95.10
lxc start simulation-container && lxc exec simulation-container -- dhclient -v

# Stage 2
lxc stop simulation-container
lxc network attach icszone simulation-container eth3 eth3
lxc config device set simulation-container eth3 ipv4.address 192.168.95.11
lxc start simulation-container && lxc exec simulation-container -- dhclient -v

# Stage 3
lxc stop simulation-container
lxc network attach icszone simulation-container eth4 eth4
lxc config device set simulation-container eth4 ipv4.address 192.168.95.12
lxc start simulation-container && lxc exec simulation-container -- dhclient -v

lxc stop simulation-container
lxc network attach icszone simulation-container eth5 eth5
lxc config device set simulation-container eth5 ipv4.address 192.168.95.13
lxc start simulation-container && lxc exec simulation-container -- dhclient -v

lxc stop simulation-container
lxc network attach icszone simulation-container eth6 eth6
lxc config device set simulation-container eth6 ipv4.address 192.168.95.14
lxc start simulation-container && lxc exec simulation-container -- dhclient -v

lxc stop simulation-container
lxc network attach icszone simulation-container eth7 eth7
lxc config device set simulation-container eth7 ipv4.address 192.168.95.15
lxc start simulation-container && lxc exec simulation-container -- dhclient -v

# The final output should look like this. You may need to restart the container and re-run dhclient -v to get this.
root@vicsort:/home/vicsort/vicsort/setup_files# lxc list
+----------------------+---------+-----------------------+------+-----------+-----------+
|         NAME         |  STATE  |         IPV4          | IPV6 |   TYPE    | SNAPSHOTS |
+----------------------+---------+-----------------------+------+-----------+-----------+
| attacker-container   | RUNNING | 192.168.90.156 (eth1) |      | CONTAINER | 0         |
+----------------------+---------+-----------------------+------+-----------+-----------+
| hmi-container        | RUNNING | 192.168.90.5 (eth1)   |      | CONTAINER | 0         |
+----------------------+---------+-----------------------+------+-----------+-----------+
| plc-container        | RUNNING | 192.168.95.2 (eth1)   |      | CONTAINER | 0         |
+----------------------+---------+-----------------------+------+-----------+-----------+
| simulation-container | RUNNING | 192.168.95.15 (eth7)  |      | CONTAINER | 0         |
|                      |         | 192.168.95.14 (eth6)  |      |           |           |
|                      |         | 192.168.95.13 (eth5)  |      |           |           |
|                      |         | 192.168.95.12 (eth4)  |      |           |           |
|                      |         | 192.168.95.11 (eth3)  |      |           |           |
|                      |         | 192.168.95.10 (eth2)  |      |           |           |
+----------------------+---------+-----------------------+------+-----------+-----------+

~~~~~~~

### Provisioning Simulation Container

~~~~~bash
# This script is in the testbed_scripts folder and simply installs afew base packages to the containers
python3 provision_lxd.py

lxc exec simulation-container bash
apt-get install libjsoncpp-dev liblapacke-dev python python-pip build-essential git dbus -y

git clone https://github.com/Fortiphyd/GRFICSv2
cd ~/GRFICSv2/simulation_vm/simulation
make

# Exit out of the container
exit

cd /home/vicsort/vicsort/setup_files/python_packages

# IF the contents of this folder are zipped, you need to unzip them
for file in *.zip; do unzip $file; done
rm *.zip

# Ensure you have the /usr/lib/python2.7/dist-packages/ path available. If you dont, then python2 isn't installed.
lxc file push -r * simulation-container/usr/lib/python2.7/dist-packages/

# Log back into the container
lxc exec simulation-container bash
cd /root/GRFICSv2/simulation_vm/simulation/remote_io/modbus

# Change the line below from /home/user to /root
nano run_all.sh
    sim_path=/root/GRFICSv2/simulation_vm/simulation/remote_io/modbus

# Create the script below with the contents provided. Eliminate the spaces when you copy and paste to your terminal.
cd /root && nano startup.sh
#! /bin/sh
pkill python3
pkill simulation
cd /root/GRFICSv2/simulation_vm/simulation && ./simulation &
cd /root/GRFICSv2/simulation_vm/simulation/remote_io/modbus/ && ./run_all.sh > /dev/null 2>&1
sleep 1
exit

chmod +x startup.sh
~~~~~
The `startup.sh` script shall be run by the `testbed_startup.py` script. However, you can test the `startup.sh` script to
ensure its working correctly. The output should look like:

~~~~bash
root@simulation-container:~# ./startup.sh
Listener on port 55555
Waiting for connections ...
New connection , socket fd is 4 , ip is : 127.0.0.1 , port : 37538
Adding to list of sockets as 0
New connection , socket fd is 5 , ip is : 127.0.0.1 , port : 37524
Adding to list of sockets as 1
New connection , socket fd is 6 , ip is : 127.0.0.1 , port : 37588
Adding to list of sockets as 2
New connection , socket fd is 7 , ip is : 127.0.0.1 , port : 37600
Adding to list of sockets as 3
New connection , socket fd is 8 , ip is : 127.0.0.1 , port : 37612
Adding to list of sockets as 4
New connection , socket fd is 9 , ip is : 127.0.0.1 , port : 37624

# You should also have a number of listening ports
# These are the modbus sensors communicating with the PLC
# The PLC should be running too, at this stage
root@simulation-container:~# netstat -lntp
Active Internet connections (only servers)
Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name
tcp        0      0 0.0.0.0:55555           0.0.0.0:*               LISTEN      8525/./simulation
tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      302/systemd-resolve
tcp        0      0 192.168.95.13:502       0.0.0.0:*               LISTEN      8539/python
tcp        0      0 192.168.95.14:502       0.0.0.0:*               LISTEN      8537/python
tcp        0      0 192.168.95.11:502       0.0.0.0:*               LISTEN      8541/python
tcp        0      0 192.168.95.15:502       0.0.0.0:*               LISTEN      8540/python
tcp        0      0 192.168.95.12:502       0.0.0.0:*               LISTEN      8538/python
tcp        0      0 192.168.95.10:502       0.0.0.0:*               LISTEN      8536/python
~~~~~

### Setting up the Web Visualisation

~~~~bash
apt install apache2 php libapache2-mod-php -y
rm /var/www/html/index.html
cp -r ~/GRFICSv2/simulation_vm/web_visualization/* /var/www/html
~~~~

You should now be able to access the simulation web URL via [http://192.168.95.10](http://192.168.95.10) from the
Host Machine's web browser. If you run into a `Your broswer does not support WebGL` error on Firefox, as you try to access the simulation URL,
simply [install Google Chrome](https://linuxize.com/post/how-to-install-google-chrome-web-browser-on-ubuntu-20-04/) and that should solve the issue.

!!! Note
   Getting the Simulation-container working was very challenging and took abit of time to figure out. I document this in the section below.

### Challenges with Unity Build
**Environment**: Note that the container I was working with was an Ubuntu 20.04 LTS container. When I attempted to start the Unity WebGL server, the build would get stuck at *Connecting to server*.
On looking at the javascript console logs, I noticed that there see,ed to be some sort of permissions issue whilst accessing the data file. However, I ruled this out as the data directory
only contained one file with instructions on how to pick data from the pymodbus sensors and push it to the simulation.


![webgl_error](../figures/webgl_error_log.png)

However, this **./Runtime/Export/Debug/Debug.binding.h Line:35** kept popping up in the logs. I tried to isolate the issue by spinning up the server. I copied the same build files to
another instance and span up an http server to serve the index.html page. However, the results were still the same. This time round though, I noticed a JSON parse issue highlighted by the **invalid value** error.
It should also be noted that system utilisation significantly increased as the web browser(Google Chrome) tried to deal with this issue with a number of requests being exchanges between the server and browser.

![webgl_error_1](../figures/webgl_error_1.png)

I googled this notorious **./Runtime/Export/Debug/Debug.binding.h Line:35** string once again hoping to catch something that I may have missed in previous searches. I came across this [Unity Cs Reference]("https://github.com/Unity-Technologies/UnityCsReference/blob/master/Runtime/Export/Debug/") which appears to be the debug.binding file before its compiled. I notice that there is a deprecated **debug.deprecated.cs** file. Looking at the deprecated components, they include some method and function calls as well as function arguments.

It's worth noting that the original VM provided in the [GRFICSv2](https://github.com/Fortiphyd/GRFICSv2) repo would correctly load the simulation. I investigated the VM to see why it was working within that VM.
Looking through the various files in the VM didn't reveal anything out of the ordinary. The though then crossed my mind. If debug.bindings.h now has a new version and the older version was deprecated, could it be that
the build was built on the older version and now is somehow not supported by either the web browser or possibly the OS. As far as I could tell, the only difference was that my VM was running Ubuntu 20.04 LTS and the original VM build was running Ubuntu 16.

Well, worth a try, just to rule that out. I span up a VM with Ubuntu 16 and configured it just as I had done with the Ubuntu 20 VM, and gave it a shot. Instantly this worked. Boo yeahhhh!!!!.

This was a strange one though. It would appear that this **debug.binding.h** file possibly relied on other C files within the host OS to successfully load the build. It would also appear that these files,
whichever they are have since been modified in the newer Ubuntu builds - and I suppose corresponding updates have been made by Unity. However, since my build is based off an old build, it would stand to reason that I need
an older OS to successfully run it.




## Workstation Container


The workstation PC is located in the ICS zone - **192.168.95.0/24**, with a static IP address of **192.168.95.5**.
The files to be setup on this machine are OpenPLC files and this machine
hosts OpenPLC Editor for editing the PLC control logic. A guide on the workstation setup [here](https://github.com/Fortiphyd/GRFICSv2/tree/master/workstation_vm).

~~~bash
lxc launch images:ubuntu/focal/amd64 workstation-container -p icszone-profile

lxc stop workstation-container
lxc config device add simulation-container eth1 none
lxc network attach icszone workstation-container eth1 eth1
lxc config device set workstation-container eth1 ipv4.address 192.168.95.5
lxc start workstation-container && lxc exec workstation-container -- dhclient -v

# Optional step to install a few base packages on the new container
./provision_lxd.py

lxc exec workstation-container bash
apt update && apt upgrade -y

# It might be necessary to have a GUI to interface with OpenPLC Editor though it appears that there is a CLI for that particular application
apt install lubuntu-desktop xrdp xfce4 -y

adduser worker # Password: worker
usermod -aG sudo worker
sed -i '1 i\TERM=xterm-256color' /home/worker/.bashrc
echo "export DISPLAY=:0" >> /home/worker/.bashrc
sh -c "echo 'Set disable_coredump false' > /etc/sudo.conf"

# Open /etc/xrdp/startwm.sh and comment out or remove the following lines, then add the line below:
nano /etc/xrdp/startwm.sh
    exec /usr/bin/startxfce4
    # test -x /etc/X11/Xsession && exec /etc/X11/Xsession
    # exec /bin/sh /etc/X11/Xsession

echo xfce4-session > /home/worker/.xsession
chown worker:worker /home/worker/.xsession
systemctl restart xrdp

apt install python -y
pip install matplotlib lxml zeroconf six numpy==1.14.6

# Install OpenPLC Editor
su worker && cd ~
sudo mkdir /root/.local/share/applications
git clone https://github.com/thiagoralves/OpenPLC_Editor
cd OpenPLC_Editor
./install.sh

cd .. && mkdir -p OpenPLC_Files && cd OpenPLC_Files
wget -L --no-check-certificate https://raw.githubusercontent.com/Fortiphyd/GRFICSv2/master/workstation_vm/attack.st
wget -L --no-check-certificate https://raw.githubusercontent.com/Fortiphyd/GRFICSv2/master/workstation_vm/attack.xml
wget -L --no-check-certificate https://raw.githubusercontent.com/Fortiphyd/GRFICSv2/master/workstation_vm/simplified_te.st
wget -L --no-check-certificate https://raw.githubusercontent.com/Fortiphyd/GRFICSv2/master/workstation_vm/simplified_te.xml

# It should be noted however that these downloaded files are meant to be fed into the **OpenPLC Web Interface** within the *plc-container*.
# You would need to follow the guide below to actually create these files. Modifying them is done using OpenPLC Editor, thus the purpose of this workstation.
~~~~~

This guide shows how to [get started](https://www.openplcproject.com/reference/basics/first-project.html) with OpenPLCEditor.
To run OpenPLC Editor, you will need to RDP into the Host Machine, and from the Host Machine, you will RDP into the workstation-container.
Launch **QTerminal** from **Applications >> Systems >> QTerminal** and use the command `xfreerdp /v:192.168.95.5 /u:worker` to get RDP
access to the workstation-container.

To launch OpenPLC Editor, use the command `cd OpenPLC_Editor && ./openplc_editor.sh` within the OpenPLC_Editor folder.

### X Display Errors
Whilst RDP'd into the workstation-contiainer from the Host Machine, I ran into an `Unable to access the X Display, is $DISPLAY set properly?` error whilst trying to
launch OpenPLC Editor with the command `./openplceditor.sh`.

Troubleshooting revealed the following:

~~~~~bash
worker@workstation-container:~/OpenPLC_Editor$ gtk-launch OpenPLC_Editor.desktop
Unable to init server: Could not connect: Connection refused

(gtk-launch:4232): Gtk-WARNING **: 08:59:46.354: cannot open display: :0

worker@workstation-container:~/OpenPLC_Editor$ echo $DISPLAY
:0

~~~~~

From my analysis, I suspect that `:0` Display interface was in use by another application, and so was `:1`. Setting `xhost su:localhost:root` like we did
in Section [PfSense Firewall](#pfsense-firewall) was not successful. After further troubleshooting, I was able to fix the problem but setting the `$DISPLAY` environment
variable to `:10`.

~~~~bash
worker@workstation-container:~/OpenPLC_Editor$ export DISPLAY=:10
worker@workstation-container:~/OpenPLC_Editor$ echo $DISPLAY
:10

# Now openplc_editor successfully launches.
worker@workstation-container:~/OpenPLC_Editor$ ./openplc_editor.sh
~~~~~

This is a temporary fix though and you would need to set this variable each time the container restarts.

To [permanently](https://stackoverflow.com/questions/13046624/how-to-permanently-export-a-variable-in-linux) set `$DISPLAY=:10`:

~~~bash
# Run this commands as `worker` user
# Add the following line and save.
# Comment out any other display exports you find there
nano ~/.bashrc
  # export DISPLAY=:0
  export DISPLAY=:10

source ~/.bashrc
~~~~



## VICSORT Initialisation script

At this stage, we have the individual components of the testbed working just fine. However, when the host machine is rebooted, the containers as well as some of the individual applications that run inside them need to be initialised. An initialisation script has already been prepared for this purpose and is stored in as `vicsort/testbed_scripts/testbed_startup.py`.

### Installing required Python Packages

You will need to install afew python packages before this script can be used.

````bash
cd testbed_scripts
pip install -r requirements.txt
````

### Running the Initialisation Script

Assuming the host machine has just been turned on, the initialisation script can be run using

~~~~bash
cd testbed_scripts
./testbed_startup.py

~~~~


### What does this script do?

This script goes through the following steps:

1. Loops through all the LXD containers obtaining their corresponding static IP addresses in reference to the network topology.
2. Start the PfSense KMV VM.
3. Provision the HMI Container.
4. Provision the OpenPLC Container.
5. Provision the Simulation Container.
6. Set the same timezone for all the containers.
7. Configure the Default routes to point all traffic from the various containers to the firewall.


## Installing Custom VICSORT motd

A custom Message of The Day (motd) banner has been designed for this testbed. The banner shows basic information about
the VICSORT host machine such as:

- CPU Information
- Memory Information
- Disk Information
- Network Information
- System Information

This banner is displayed when you log onto the machine whilst in server mode (not GUI mode) or when you SSH into the machine.


![baner](../figures/custom_banner_1.png)

### Setting up the custom banner

A script has been created to setup this banner and is available in the `vicsort/testbed_scripts/` directory.
The script must be run as **root**.
~~~bash
cd ~/vicsort/testbed_scripts
python3 configure_custom_banner.py
~~~


## Restarting VICSORT

It might be necessary to reset or restart the testbed without having to reboot the machine.
Since the testbed is mainly comprised of two components namely: LXD and KVM, with the bulk of the testbed in LXD,
restarting LXD should reset the testbed. You would need to go through the intialisation process again upon doing this.

LXD is typically installed using snap. If your LXD installation is snap based, you can restart LXD using

~~~bash
# This command takes a while if you had containers with a number of active processes running
systemctl restart snap.lxd.daemon.service
~~~

If you are not sure with process is controlling your LXD, run the command `systemctl -a | grep LXD` and look for all
processes marked as **running**.

Note that when you restart LXD, its generally good practice to restart the PfSense KVM VM with the commands `virsh shutdown pfsense-vm` and `virsh start pfsense-vm`,
or simply `virsh reboot pfsense-vm`.

<br><br>
******
Version 1.2, Feb 2024

By Conrad Ekisa
******