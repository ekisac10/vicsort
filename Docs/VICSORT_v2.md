# VICSORT v2

This guide documents the upgrades made to the initial vicsort v1 `ova` file - a sort of changelog.
This is intended to make the testbed easier to use and fix bugs within the previous version.


## VM Upgrades


1. The size on the VM has been increased from 48GB to 65GB to allow more space to the user.


## Scripts Upgrades


1. The alias `testbed_startup` has been created for `./home/vicsort/Desktop/vicsort/testbed_scripts/testbed_startup.py`. Therfore,
   to start the testbed, simply run `testbed_startup` in the terminal.
2. Added `modify_resolv.py` script to the `attacker-container` and a cron job to ensure that the DNS servers are always correctly set.
3. Created alias `rdp_attacker` on VICSORT host. When executued, this opens an RDP session to the *attacker-container*.
4. Created alias `rdp_workstation` on VICSORT Host. When executed, this opens an RDP session to the *workstation-container*.
5. Created alias 'openplc_editor' on `workstation-container` to launch OpenPLC Editor.



## Software Upgrades


1. Added Chrome to the quicklaunch menu. Chrome also opens up with the 4 relevant tabs to the VICSORT testbed.
2. Updated `attacker-container` packages. Always recommended to update the packages before use.
3. Fixed RDP to attacker-container and launching the *wireshark* GUI as root within the RDP session
4. Added modified versions of the PLC ladder logic files that are editable using OpenPLC Editor on the `workstation-container`.
   These are added under `setup_files/OpenPLC_files`.
5. Installed `terminator`. Great tool for working with multiple terminals within the same window.




<br>

<hr>
Version 2.0, Jan 2024<br>
© Conrad Ekisa