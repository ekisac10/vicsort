#! /bin/python
# Script to design custom banner
# A useful resource in the preparation of this script: https://www.thepythoncode.com/article/get-hardware-system-information-python

from termcolor import colored, cprint
import psutil
import platform
import os
import math
import netifaces as ni
from datetime import datetime
from prettytable import PrettyTable

banner = """

██╗   ██╗  ██╗   ██████╗  ███████╗   ██████╗    ██████╗   ████████╗
██║   ██║  ██║  ██╔════╝  ██╔════╝  ██╔═══██╗   ██╔══██╗  ╚══██╔══╝
██║   ██║  ██║  ██║       ███████╗  ██║   ██║   ██████╔╝     ██║   
╚██╗ ██╔╝  ██║  ██║       ╚════██║  ██║   ██║   ██╔══██╗     ██║   
 ╚████╔╝   ██║  ╚██████╗  ███████║  ╚██████╔╝   ██║  ██║     ██║   
  ╚═══╝   ╚═╝   ╚═════╝╚  ══════╝   ╚═════╝ ╚   ═╝  ╚═╝     ╚═╝   
"""

banner1 = "                   >> a Virtualised ICS OpenSource Research Testbed"


author_info = {"Build by": "Conrad Ekisa", "Version": "v2.0", "Gitlab Repo": "https://gitlab.com/ekisac10/vicsort", "Email": "ekisac10@gmail.com"}

def print_author_info(info: dict):
    key_length = 0
    for item in info.keys():
        if len(item) > key_length:
            key_length = len(item)
    for key, value in info.items():
        print(colored(key, "red", attrs=["bold"]) + " "*(key_length-len(key)) + "  :  " + colored(value, "green", attrs = ["bold"]))
    print("")


banner_end = "+"*110

def get_size(bytes, suffix="B"):
    """
    Scale bytes to its proper format
    e.g:
        1253656 => '1.20MB'
        1253656678 => '1.17GB'
    """
    factor = 1024
    for unit in ["", "K", "M", "G", "T", "P"]:
        if bytes < factor:
            return f"{bytes:.2f}{unit}{suffix}"
        bytes /= factor

cprint(banner_end, "green")
print(colored(banner, "magenta"))
print(colored(banner1, "cyan", attrs=[ "bold"]))
print("\n")
print_author_info(author_info)


def system_info():
    uname = platform.uname()
    system = color_description_text("System: ") + color_result_text(uname.system)
    time = color_description_text("Current Time: ") + color_result_text(datetime.now().strftime("%A %d %b %Y %H:%M:%S"))
    operating_sys = color_description_text("Operating System: ") + color_result_text(os.popen("lsb_release -s -d").read()[:-1])
    node_name = color_description_text("Node Name: ") + color_result_text(uname.node)
    processor = color_description_text("Processor: ") + color_result_text(uname.processor)
    user_logged_on = color_description_text("Current User Logged on: ") + color_result_text(os.popen("whoami").read()[:-1])
    result = "{}\n{}\n{}\n{}\n{}\n{}".format(system, time, operating_sys, node_name, processor, user_logged_on)
    return result


def generate_title_headers(title: str, text_color = "magenta"):
    # Default Column header width is 50 characters
    title_length = len(title)
    no_of_equal_signs = 49 - title_length
    either_side = no_of_equal_signs / 2
    if (either_side % 2) == 0:
        print_output = colored("="* int(either_side) + " {} ".format(title) + "="*int(either_side), text_color, attrs = ["bold"])
    else:
        print_output = colored("="*math.ceil(either_side) + " {} ".format(title) + "="*math.ceil(either_side), text_color, attrs=["bold"])
    return print_output


def generate_title_headers_3(title: str, text_color = "magenta"):
    title_length = len(title)
    no_of_equal_signs = (95 / 3) - title_length
    either_side = no_of_equal_signs / 2
    if (either_side % 2) == 0:
        print_output = colored("="* int(either_side) + " {} ".format(title) + "="*int(either_side), text_color, attrs = ["bold"])
    else:
        print_output = colored("="*math.floor(either_side) + " {} ".format(title) + "="*math.ceil(either_side), text_color, attrs=["bold"])
    return print_output


def color_description_text(text: str, color="red"):
    return colored(text, color, attrs = ["bold"])

def color_result_text(text: str, color = "green"):
    return colored(text, color, attrs = ["bold"])

def cpu_info():
    physical_cores = color_description_text("Physical cores: ") + color_result_text(psutil.cpu_count(logical=False))
    total_cores = color_description_text("Total cores: ") + color_result_text(psutil.cpu_count(logical=True))
    cpu_usage_header = colored("CPU Usage Per Core: ", "white", attrs = ["bold"])
    utilisation = []
    for i, percentage in enumerate(psutil.cpu_percent(percpu=True, interval=1)):
        core = color_description_text("Core {}: ".format(i)) + color_result_text("{}%".format(percentage))
        utilisation.append(str(core))
    total_cpu = color_description_text("Total CPU Usage: ") + color_result_text(psutil.cpu_percent())
    result = "{}\n{}\n\n{}\n{}\n\n{}".format(physical_cores, total_cores, cpu_usage_header, "\n".join(utilisation), total_cpu)
    return result


def mem_info():
    svmem = psutil.virtual_memory()
    total_mem = color_description_text("Total: ") + color_result_text(get_size(svmem.total))
    avail_mem = color_description_text("Available: ") + color_result_text(get_size(svmem.available))
    used_mem = color_description_text("Used: ") + color_result_text(get_size(svmem.used))
    percentage_used = color_description_text("Percentage Used: ") + color_result_text("{}%".format(svmem.percent))
    result = "{}\n{}\n{}\n{}".format(total_mem, avail_mem, used_mem, percentage_used)
    return result

def network_info():
    ip_mapping = {}
    for interface in ni.interfaces():
        try:
            ip_address = ni.ifaddresses(interface)[2][0]['addr']
            ip_mapping[interface] = ip_address
        except Exception as e:
            pass
    longest_interface = 0
    for k in ip_mapping.keys():
        if len(k) > longest_interface:
            longest_interface = len(k)
    result = []
    for k, v in ip_mapping.items():
        result.append(color_description_text("IPv4 Address for ") + color_description_text("{}: ".format(k)) +
                      " "*(longest_interface - len(k)) + color_result_text("{}".format(v)))
    result = "\n".join(result)
    return result

def disk_info():
    intro = ""
    device = ""
    mount = ""
    file_sys = ""
    used = ""
    free = ""
    percentage_used = ""
    partitions = psutil.disk_partitions()
    for partition in partitions:
        if partition.device == "/dev/root" or "/dev/sda2":
            intro = colored("Partitions and Usage: ", "white", attrs = ["bold"])
            device = colored("=== Device: {} ===".format(partition.device) , "white", attrs=["bold"])
            mount = "   {} {}".format(color_description_text("Mountpoint:"), color_result_text(partition.mountpoint))
            file_sys = "   {} {}".format(color_description_text("File System type:"), color_result_text(partition.fstype))
            try:
                partition_usage = psutil.disk_usage(partition.mountpoint)
            except PermissionError:
                # this can be catched due to the disk that
                # isn't ready
                continue
            used = "   {} {}".format(color_description_text("Used:"), color_result_text(get_size(partition_usage.total)))
            free = "   {} {}".format(color_description_text("Free:"), color_result_text(get_size(partition_usage.free)))
            percentage_used = "   {} {}".format(color_description_text("Percentage Used:"), color_result_text(partition_usage.percent))
            break
    result = "{}\n{}\n{}\n{}\n{}\n{}\n{}".format(intro, device, mount, file_sys, used, free, percentage_used)
    return result



cpu_mem_table = PrettyTable([generate_title_headers_3("CPU Information"), generate_title_headers_3("Memory Information"), generate_title_headers_3("Disk Information")])
cpu_mem_table.align[generate_title_headers_3("CPU Information")] = "l"
cpu_mem_table.align[generate_title_headers_3("Memory Information")] = "l"
cpu_mem_table.align[generate_title_headers_3("Disk Information")] = "l"
cpu_output = cpu_info()
mem_output = mem_info()
disk_output = disk_info()
cpu_mem_table.add_row([cpu_output, mem_output, disk_output])

network_disk_table = PrettyTable([generate_title_headers("Network Information"), generate_title_headers("System Information")])
network_disk_table.align[generate_title_headers("System Information")] = "l"
network_disk_table.align[generate_title_headers("Network Information")] = "l"
sys_output = system_info()
network_output = network_info()
network_disk_table.add_row([network_output, sys_output])

print(cpu_mem_table)
print("\n")
print(network_disk_table)
cprint(banner_end, "green")
print("\n")

