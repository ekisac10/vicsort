#! /usr/bin/python3
import os

os.system("lxc list")
container_name = input("\nEnter name of container to provision: ")

base_command = "lxc exec {} -- "

packages_to_install = "apt install nano git curl wget python3-pip unzip net-tools iputils-ping -y"
update_and_upgrade = "apt update && apt upgrade -y"
dhcp = "dhclient -v"

os.system(base_command.format(container_name) + dhcp)
os.system(base_command.format(container_name) + update_and_upgrade)
os.system(base_command.format(container_name) + packages_to_install)
