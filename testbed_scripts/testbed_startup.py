#! /usr/bin/python3
# This script runs necessary commands to bring the containers to a ready state on host reboot.

import os
from termcolor import colored
from time import sleep
from subprocess import Popen, PIPE

# ENVIRONMENT VARIABLES
timezone = "Europe/Dublin"

if os.getuid() != 0:
    print(colored("\nScript must be run as root. Exiting ...\n", "red", attrs=["bold"]))
    exit(0)

container_list = ["hmi-container", "plc-container", "simulation-container", "workstation-container",
                  "attacker-container"]
container_zones = {"hmi-container": "WAN", "plc-container": "LAN", "simulation-container": "LAN",
                   "workstation-container": "LAN", "attacker-container": "WAN"}
vm_list = ["pfsense-vm"]


def get_ip_addresses(containers: list):
    command = "lxc exec {} -- dhclient -v"
    command1 = "lxc list | awk '$3' | grep eth7"
    print(colored("\nObtaining IP addresses for ICS LXD Containers\n", "green"))
    try:
        for container in containers:
            print(colored("Obtaining IP addresses for {}".format(container), "cyan"))
            os.system(command.format(container))
            # Check that simulation-container has picked up all required IP addresses
            if container == "simulation-container" and os.popen('{}'.format(command1)).read() == "":
                os.system(command.format("simulation-container"))
            if container == "simulation-container" and os.popen("lxc list | grep 192.168.95.11").read() == "":
                os.system(command.format("simulation-container"))
            print(colored("Obtained IP addresses for {}.\n".format(container), "green"))
        show_lxc_state()
    except Exception as e:
        print(e)


def show_lxc_state():
    os.system("lxc list")
    print("\n")


def show_kvm_state():
    os.system("virsh list --all")
    print("\n")


def start_vms(vms: list):
    # This starts the kvm VMS

    for vm in vms:
        print(colored("Starting VM: {} now".format(vm), "green"))
        try:
            os.system("virsh start {}".format(vm))
            print(colored("VM: {} started successfully. ".format(vm), "green"))
        except Exception as e:
            print(colored("{}: {}".format(vm, e)))
    show_kvm_state()


def set_container_timezone(containers: list):
    print(colored("\nSetting timezones in containers\n", "cyan"))
    for container in containers:
        try:
            os.system("lxc exec {} -- timedatectl set-timezone {}".format(container, timezone))
            print(colored("Successfully set timezone for {} to {}\n".format(container, timezone), "green"))
        except Exception as e:
            print(
                colored("An error occurred while setting the timezone for {} to {}".format(container, timezone), "red"))


def check_container_state(container: str):
    status = os.popen("lxc list -c ns --format=csv | grep {}".format(container)).read()
    if status != "":
        status = status.rstrip().split(",")
        return status[1]
    else:
        print(colored("ERROR: Container: {} not found".format(container), "red"))
        return None


def start_stopped_containers(container: str):
    status = check_container_state(container)
    if status == "STOPPED":
        os.system("lxc start {}".format(container))
        print(colored("\nContainer: {} was Stopped. It's been started...".format(container), "green"))


def provision_hmi(hmi_container: str):
    print(colored("Provisioning HMI Container", "cyan"))
    os.system("lxc exec {} -- pkill java".format(hmi_container))
    try:
        process = Popen("lxc exec {} bash".format(hmi_container), shell=True, stdin=PIPE, stdout=PIPE,
                        stderr=PIPE)
        process.communicate(bytes("cd /opt/tomcat6/apache-tomcat-6.0.53/bin && ./startup.sh", "utf-8"), timeout=2)
    except Exception as e:
        print(colored(e, "green"))
    # os.system("lxc exec {} -- /opt/tomcat6/apache-tomcat-6.0.53/bin/startup.sh".format(hmi_container))
    sleep(0.5)
    os.system("\nlxc exec {} -- netstat -lntp".format(hmi_container))
    print(colored("ScadaBR started. Access web UI at http://192.168.90.5:9090/ScadaBR\n", "green"))


def provision_plc(plc_container: str):
    print(colored("Provisioning OpenPLC Container", "cyan"))
    try:
        process = Popen("lxc exec {} bash".format(plc_container), shell=True, stdin=PIPE, stdout=PIPE,
                        stderr=PIPE)
        process.communicate(bytes("cd /root && python3 provision_plc.py ", "utf-8"), timeout=2)
    except Exception as e:
        pass
    # Check if OpenPLC is running
    os.system("lxc exec {} -- ./provision_plc.py".format(plc_container))
    print(colored("OpenPLC Running. Check PLC status at http://192.168.95.2:8080\n", "green"))


def provision_simulation(simulation_container: str):
    print(colored("Provisioning Simulation Container", "cyan"))
    try:
        process = Popen("lxc exec {} bash".format(simulation_container), shell=True, stdin=PIPE, stdout=PIPE,
                        stderr=PIPE)
        process.communicate(bytes("cd /root && ./startup.sh ", "utf-8"), timeout=2)
    except Exception as e:
        pass
    print(colored("\nRunning Processes on Simulation-container", "magenta"))
    os.system("\nlxc exec {} -- netstat -lntp".format(simulation_container))
    print(colored("Simulation Container ready for use", "green"))


def clear_default_route(container: str):
    os.system("lxc exec {} -- ip route del default".format(container))
    print(colored("Cleared default route for {}".format(container), "green"))


def set_default_routes(container: str, zone: str):
    """
    Accepted Zones are WAN (DMZ Zone, 192.168.90.0/24) and the LAN (ICS Zone, 192.168.95.0/24)
    Default route for WAN is 192.168.90.100
    Default route for LAN is 192.168.95.100
    """
    if zone != "WAN" and zone != "LAN":
        print(colored("Invalid zone provided. Should be WAN or LAN. Not configuring default routes", "red"))
        return 0
    clear_default_route(container)
    if zone == "WAN":
        try:
            if container == "attacker-container":
                os.system("lxc exec {} -- ip route add default via 192.168.90.1".format(container))
                print(colored("Set default route for {}, on the WAN to to 192.168.90.1\n".format(container), "green"))
            else:
                os.system("lxc exec {} -- ip route add default via 192.168.90.100".format(container))
                print(colored("Set default route for {}, on the WAN to to 192.168.90.100\n".format(container), "green"))
        except Exception as e:
            print(colored("An error occurred while setting default routes for {}: {}".format(container, e), "red"))
    if zone == "LAN":
        try:
            os.system("lxc exec {} -- ip route add default via 192.168.95.100".format(container))
            print(colored("Set default route for {}, on the LAN to to 192.168.95.100\n".format(container), "green"))
        except Exception as e:
            print(colored("An error occurred while setting default routes for {}: {}".format(container, e), "red"))


def provision_containers():
    provision_hmi(container_list[0])
    provision_plc(container_list[1])
    provision_simulation(container_list[2])
    set_container_timezone(container_list)
    print(colored("\nConfiguring Default Routes to work with pfSense firewall\n", "cyan"))
    for container_name, zone in container_zones.items():
        set_default_routes(container_name, zone)


def main():
    for containers in container_list:
        start_stopped_containers(containers)
    get_ip_addresses(container_list)
    start_vms(vm_list)
    provision_containers()


if __name__ == "__main__":
    main()
    sleep(1)
    print(colored("\n**** Testbed Ready to go ****\n", "cyan"))
