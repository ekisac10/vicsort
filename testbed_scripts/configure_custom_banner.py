#! /bin/python

from termcolor import colored
import os
#
if os.getuid() != 0:
    print(colored("\nScript must be run as root. Exiting ...\n", "red", attrs=["bold"]))
    exit(0)

directory = os.getcwd()
while "testbed_scripts" not in directory:
    print(colored("Its seems like you are not in the testbed_scripts dir. The script may not work. Hit ENTER to continue anyway or Q to quit", "red"))
    user_input = input(">>> ")
    if user_input == "Q":
        print("Exiting script now !!!")
        exit()
    if user_input == "q":
        print(colored("Exiting script now !!!\n", "green"))
        exit()
    if user_input == "":
        break

print(colored("\nDisabling all current default MOTD's Daemon scripts ...", "green"))
os.system("chmod -x /etc/update-motd.d/*")

print(colored("\nCopying custom banner scripts to MOTD default dir: /etc/update-motd.d/ ...", "green"))
try:
    os.system("rm /etc/update-motd.d/01-custo* 2>/dev/null")
except Exception as e:
    pass
os.system("cp 01-custom /etc/update-motd.d/")
os.system("cp 01-custom.py /etc/")

print(colored("\nMaking new script executable ...", "green"))
os.system("chmod +x /etc/update-motd.d/01-custom")
os.system("chmod +x /etc/01-custom.py")

print(colored("\nYeap, that's it. All good to go !!!\n", "green"))

# Solution from https://stackoverflow.com/questions/41088877/update-motd-d-scripts-not-running
print(colored("""
If you are still not able to see the modified motd upon next ssh login, run the command below to identify the
problem with the script execution process:

run-parts /etc/update-motd.d/ > /dev/null\n
""", "green", attrs=["bold"]))
