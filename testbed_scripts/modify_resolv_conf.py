#! /usr/bin/env python3

import os


"""
This script modifies the DNS file info and sets DNS server to 8.8.8.8.
If run with a cron job, it will ensure DNS is always 8.8.8.8.
"""


if os.getuid() != 0:
    print("\nScript must be run as root. Exiting ...\n")
    exit(0)

dns_servers = set()
new_dns_server = 'nameserver 8.8.8.8'

# Open /etc/resolv.conf and read its contents
with open('/etc/resolv.conf', 'r') as f:
    lines = f.readlines()

# Loop through each line and look for existing DNS servers
for line in lines:
    if line.startswith('nameserver'):
        dns_servers.add(line.strip())

# Remove any DNS servers that are not 8.8.8.8
for dns_server in dns_servers:
    if dns_server != new_dns_server:
        lines.remove(dns_server + '\n')

# If 8.8.8.8 is not in the file, add it
if new_dns_server + '\n' not in lines:
    lines.append(new_dns_server + '\n')

# Write the modified contents back to /etc/resolv.conf
with open('/etc/resolv.conf', 'w') as f:
    f.writelines(lines)

